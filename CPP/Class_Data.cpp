#include<iostream>
using namespace std;

class Data
{
public:
	// 1.全缺省的构造函数——采用初始化队列直接初始化
	// 使用explicit关键字：声明类构造函数是显示调用的，而非隐式调用(用于单参构造函数或缺省函数)
	// （详见函数 Data& operator-=(int day)）
	explicit Data(int year = 1900, int month = 1, int day = 1)  //缺省参数
		: _year(year)
		, _month(month)
		, _day(day)
	{
		// 数据合法性判定
		if (!(year > 0 &&
			(month > 0 && month <= 12) &&
			(day > 0 && day <= GetMonthDay(year, month))))
		{
			// 若不合法直接初始化值
			_year = 1800;
			_month = 1;
			_day = 1;
		}
	}

	// 2.拷贝构造函数——采用初始化队列直接拷贝初始化
	Data(const Data& d)
		: _year(d._year)
		, _month(d._month)
		, _day(d._day)
	{
		// 拷贝构造函数无需进行合法性判断
	}

	// 3.析构函数——该项目析构函数无需特殊操作
	~Data()
	{}

	// 4.赋值运算符重载  d2 = d3 等价于 d2.operator=(&d2, d3)
	Data& operator=(const Data& d)
	{
		if (this != &d)  //防止自我赋值
		{
			_year = d._year;
			_month = d._month;
			_day = d._day;
		}
		return *this;
	}
	//---------------------------------------------------------------------------------------------------------------------
	// 5.打印日期
	//const修饰参数，使该函数不能改变引用参数的值——因为静态函数不允许使用const函数
	//static修饰，表明为静态成员函数
	inline static void printData(const Data& d)//const
	{
		cout << d._year << "-" << d._month << "-" << d._day << endl;
	}

	// 6.private成员变量对外接口函数
	inline void set_year(int a)  //设置year值
	{
		if (a > 0)
		{
			_year = a;
		}
		else
		{
			cout << "输入数值不合法！" << endl;
		}
	}
	inline int get_year()const  //获取year值
	{
		return _year;
	}

	inline void set_month(int a)  //设置month值
	{
		if (a > 0 && a <= 12)
		{
			_month = a;
		}
		else
		{
			cout << "输入数值不合法！" << endl;
		}
	}
	inline int get_month()const  //获取month值
	{
		return _month;
	}

	inline void set_day(int a)  //设置day值
	{
		if (a > 0 && a <= GetMonthDay(_year, _month))
		{
			_day = a;
		}
		else
		{
			cout << "输入数值不合法！" << endl;
		}
	}
	inline int get_day()const //获取day值
	{
		return _day;
	}
	//---------------------------------------------------------------------------------------------------------------------	
	// 7.日期+天数
	Data operator+(int day)const
	{
		// 返回某日期后day天的日期
		
		if (day < 0)  //若day为负，直接调用对象 -函数
		{
			return *this - (0 - day);
		}
		// + 操作不能改变两对象的值
		//（但这里不用const修饰函数，因为自增自减函数要调用该函数）
		Data temp(*this);
		temp._day = temp._day + day;

		// 日期换算
		int dayofMonth = 0;
		while(temp._day > (dayofMonth = GetMonthDay(temp._year, temp._month)))
		{
			temp._day -= dayofMonth;
			temp._month += 1;
			if (temp._month > 12)  //月数重置
			{
				temp._year += 1;
				temp._month = 1;
			}
		}
		return temp;
	}

	// 8.日期+=天数
	Data& operator+=(int day)
	{
		*this = *this + day;
		return *this;
	}
	//---------------------------------------------------------------------------------------------------------------------	
	// 9.日期-天数
	Data operator-(int day)const
	{
		// 返回某日期前day天的日期

		if (day < 0)  //若day为负，直接调用对象 +函数
		{
			return *this + (0 - day);
		}

		Data temp(*this);
		temp._day = temp._day - day;

		// 日期换算
		while (temp._day <= 0)
		{
			temp._month -= 1;  
			if (temp._month <= 0)  //月数重置
			{
				temp._year -= 1;
				temp._month = 12;
			}
			temp._day += GetMonthDay(temp._year, temp._month);//注意这里要先更新到前一个月再获取日期
		}
		return temp;
	}
	// 10.日期-=天数
	Data& operator-=(int day)
	{
		*this = *this - day;  
		//这里有报错！
		//【原因】：会有将整型day变量隐式类型转换为Data类型的对象的功能
		//因此：可调用两种函数size_t Data::operator -(const Data &) 与 Data Data::operator -(int) const
		//编译器无法确定到底该调用那个函数，因此报错。
		//【解决】：为构造函数添加explicit关键字
							
		return *this;
	}
	//---------------------------------------------------------------------------------------------------------------------	
	// 11.对象前置++
	Data& operator++()
	{
		// 直接加后返回新值
		*this = *this + 1;
		return *this;
	}

	// 对象后置++
	// 参数无意义，仅与前置++（--）区分，防止重载无法区分
	Data operator++(int)  
	{
		// 先使用后++：要返回旧值
		Data temp = *this;
		*this = *this + 1;
		return temp;
	}

	// 12.对象前置--
	Data& operator--()
	{
		*this = *this - 1;
		return *this;
	}
	// 对象后置--
	Data operator--(int)
	{
		Data temp = *this;
		*this = *this - 1;
		return temp;
	}
	//---------------------------------------------------------------------------------------------------------------------	
	// 13.大小运算符重载

	// >运算符重载
	bool operator>(const Data& d)const
	{
		if (this->_year > d._year)
		{
			return true;
		}
		else if (this->_year == d._year && this->_month > d._month)
		{
			return true;
		}
		else if (this->_year == d._year && this->_month == d._month && this->_day > d._day)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// >=运算符重载  //为什么要设为内联函数？
	bool operator >= (const Data& d)const
	{
		if (!operator<(d))
		{
			return true;
		}
		return false;
	}

	// ==运算符重载
	bool operator==(const Data& d)const
	{
		if (this->_year == d._year &&
			this->_month == d._month &&
			this->_day == d._day)
		{
			return true;
		}
		return false;
	}

	// !=运算符重载
	bool operator != (const Data& d)const
	{
		return !(*this == d);
	}


	// <运算符重载
	bool operator < (const Data& d)const
	{
		if (this->_year < d._year)
		{
			return true;
		}
		else if (this->_year == d._year && this->_month < d._month)
		{
			return true;
		}
		else if (this->_year == d._year && this->_month == d._month && this->_day < d._day)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// <=运算符重载
	bool operator <= (const Data& d)const
	{
		if (!operator>(d))
		{
			return true;
		}
		return false;
	}

	//---------------------------------------------------------------------------------------------------------------------
	// 14.日期-日期 返回天数(一个正数)
	size_t operator-(const Data& d)const
	{
		// 方法：让小日期+day直至等于大日期
		Data minData(*this);
		Data maxData(d);
		if (minData > maxData)
		{
			minData = d;
			maxData = *this;
		}
		int day = 0;
		while (minData != maxData)
		{
			minData++;
			day++;
		}
		return day;
	}
	
	// 15.判断是否闰年
	bool IsLeapYear(int year)const  //const修饰函数不能修改对象内部成员值
	{
		// 四年一闰 百年不闰 四百年再一闰
		if ((year % 4 == 0 && year % 100 != 0) ||
			year % 400 == 0)
		{
			return true;
		}
		return false;
	}
	//---------------------------------------------------------------------------------------------------------------------
private:
	// 16.获取某年某月的天数
	int GetMonthDay(int year, int month)const  //const修饰函数不能修改对象内部成员值
	{
		// 数组下标对应的数值为相应月份天数
		// 将数组修饰为static使其变为静态数组，声明周期为全局
		static int days[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		// 如果获取二月份的值，需判断是否闰年
		if (month == 2 && IsLeapYear(year))
		{
			// 如果为闰年 2月多一天
			return days[2]+1;
		}
		return days[month];
	}
	//---------------------------------------------------------------------------------------------------------------------
private:
	int _year;
	int _month;
	int _day;
};


int main()
{
	Data d1(2022, 3, 21);

	Data::printData(d1);
	
	//int day = d1.get_day();
	//cout << day << endl;
	//d1.set_month(13);

	Data d2 = d1 + 100;
	//Data::printData(d2);

	//d2 = d1 - 100;
	//Data::printData(d2);

	//d2 = d1 + -100;
	//Data::printData(d2);

	cout << d1 - d2 << endl;

	return 0;
}