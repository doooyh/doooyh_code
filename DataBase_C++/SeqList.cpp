#include "SeqList.h"
#include <string.h>
#pragma warning(disable:4996)


// 1.顺序表类的无参构造函数
SeqList::SeqList()   // 直接定义在初始化队列中
: _array(new DataType[3]())  //初始化动态数组所有元素为0
, _size(0)
, _capacity(3)
{}

// 2.顺序表类的有参构造函数
SeqList::SeqList(DataType* array, size_t size)
: _array(new DataType[size])
, _capacity(size)
, _size(size)
{
	for (size_t i = 0; i < size; ++i)
	{
		_array[i] = array[i];
	}
}
#if 0  //数组传参会转为指针，数组长度只能传参获得，只有数组地址无法知道
SeqList::SeqList(DataType* array) 
{
	size_t size = sizeof(array) / sizeof(array[0]);
	_array = new DataType[size];
	_size = size;
	_capacity = size;

	//memccpy(_array, array, sizeof(DataType), size);  vs中memcopy会报错，类似于_scanf
	for (size_t i = 0; i < size; ++i)
	{
		_array[i] = array[i];
	}
}
#endif

// 3.顺序表类的析构函数
SeqList::~SeqList()
{
	if (_array)  // 先判断数组是否有元素
	{
		delete[] _array;
		_array = NULL;
		_size = 0;
		_capacity = 0;
	}
}

// 4.顺序表类的拷贝构造函数
SeqList::SeqList(const SeqList& s)
: _array(new DataType[s._size])
, _size(s._size)
, _capacity(s._size)
{
	memcpy(_array, s._array, sizeof(DataType)*s._size);
}

//-------------------------------------------------------
// 5.顺序表类赋值运算符=的重载
SeqList& SeqList::operator=(const SeqList& s)
{
	if (this != &s)
	{  
		DataType* temp = new DataType[s._size];
		memcpy(temp, s._array, sizeof(DataType)*s._size);
		// 先释放原空间 
		// this->~SeqList();  若调用析构函数(不推荐，易产生双重free问题)必须加this->
		delete[] _array;
		_array = temp;
		_size = s._size;
		_capacity = s._capacity;
	}
	return *this;
}

// 6.打印顺序表
void SeqList::Print()
{
	// 判断是否有元素
	if (isEmpty())
	{
		cout << "Print Error!" << endl;
		return;
	}
	//for (auto a : _array) // ??基于范围的for循环不适用类中的函数
	//{
	//}
	for (int i = 0; i < _size; i++)
	{
		cout << _array[i] << " ";
	}
	cout << endl;
}
//-------------------------------------------------------
// 7.尾插
void SeqList::PushBack(const DataType data)
{
	// 检测是否需扩容
	CheckCapacity();
	// 尾部直接添加
	_array[_size] = data;
	// 更新对象成员
	_size++;
}
// 8.尾删
void SeqList::PopBack()
{
	// 判断是否有元素
	if (isEmpty())
	{
		cout << "PopBack Error!" << endl;
		return;
	}
	// 更新对象成员
	_size--;
}
//-------------------------------------------------------
// 9.头插
void SeqList::PushFront(DataType data)
{
	// 检测是否需要扩容
	CheckCapacity();
	// 所有元素向后移一位
	for (int i = _size ; i > 0; i--)
	{
		_array[i] = _array[i - 1];
	}
	// 头部插入元素
	_array[0] = data;
	// 更新对象成员
	_size++;
}

// 10.头删
void SeqList::PopFront()
{
	// 检测顺序表是否为空
	if (isEmpty())
	{
		cout << "PopFront Error!" << endl;
		return;
	}
	// 所有元素向前移一位
	for (int i = 0; i < _size - 1; i++)
	{
		_array[i] = _array[i + 1];
	}
	// 更新对象成员
	_size--;
}
//-------------------------------------------------------
// 11.在顺序表的pos位置插入元素data
// 注意：pos的范围必须在[0, ps->size]
void SeqList::Insert(int pos, DataType data)
{
	// 检测是否需要扩容
	CheckCapacity();
	// 判断位置是否合法
	if (pos < 0 || pos > _size)
	{
		cout << "Insert Error!" << endl;
		return;
	}
	// pos位置后（包括pos）的所有元素向后移动一个单位
	for (int i = _size; i > pos; i--)
	{
		_array[i] = _array[i - 1];
	}
	// pos位置插入元素
	_array[pos] = data;
	// 更新对象元素
	_size++;
}
// 12.顺序表删除pos位置的值
// 注意：pos的范围必须在[0, ps->size)
void SeqList::Erase(int pos)
{
	CheckCapacity();
	if (pos < 0 || pos >= _size)
	{
		cout << "Erase Error!" << endl;
		return;
	}
	for (int i = pos; i < _size; i++)
	{
		_array[i] = _array[i + 1];
	}
	_size--;
}

// 13.查找元素位置
int SeqList::SeqListFind(DataType data)
{
	for (int i = 0; i < _size; ++i)
	{
		if (_array[i] == data)
			return i;
	}
	return -1;
}

//-------------------------------------------------------
// 14.检测顺序表是否为空
bool SeqList::isEmpty()
{
	return  0 == _size;
}
//-------------------------------------------------------
// 15.获取顺序表中的第一个元素
DataType SeqList::SeqListFront()
{
	return _array[0];
}
// 16.获取顺序表中最后一个元素
DataType SeqList::SeqListBack()
{
	return _array[_size - 1];
}
// 17.获取顺序表中任意位置的元素
DataType SeqList::SeqListGet(int pos)
{
	if (pos < 0 || pos >= _size)
	{
		cout << "SeqListGet Error!" << endl;
		return -1;
	}
	return _array[pos];
}
//-------------------------------------------------------
// 18.清空顺序表
void SeqList::EmptySeqList()
{
	delete _array;
	_array = new DataType[3]();
	_size = 0;
	_capacity = 3;
}
//-------------------------------------------------------
// 19.获取类成员size
size_t SeqList::Size()const
{
	return _size;
}
// 20.获取类成员capacity
size_t SeqList::Capacity()const
{
	return _capacity;
}

//-------------------------------------------------------
void test1()
{
	SeqList s1;
	DataType arr[] = { 1, 2, 3, 4, 5 };
	SeqList s2(arr, 5);
	SeqList s3(s2);

	s1 = s2;

	s1.PushFront(0);
	s2.PushBack(6);
	s1.PopFront();
	s2.PopBack();

	s1.Print();
	s2.Print();
	s3.Print();
}

void test2()
{
	DataType arr[] = { 1, 2, 3, 4, 5 };
	SeqList s2(arr, 5);
	s2.Insert(0, 0);
	s2.Erase(5);
	cout << s2.SeqListFind(3) << endl;
}


// 测试函数
void SeqList_test()
{
	//test1();
	test2();
}