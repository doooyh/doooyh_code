// 数据结构：顺序表——cpp
// cpp就是将顺序表结构与方法全部封装在了类中，具体思路与顺序表C语言的实现差异不大
// 在头文件需对类进行定义，注意类的成员函数只是声明

//------------------------------------------------------------------------------------------------------------
#pragma once  // 头文件起始加入，保证头文件只被编译一次
#include <iostream>
#include <string.h>
using namespace std;

typedef int DataType;

class SeqList
{
private:
	
	// 1.检测顺序表空间是否扩容
	void CheckCapacity()
	{
		if (_size < _capacity)
		{
			return;
		}
		// 开辟新空间（左移一位变两倍）
		DataType* temp = new DataType[_capacity << 1];
		// 拷贝原空间
		memcpy(temp, _array, _size*sizeof(DataType));
		// 释放旧空间
		delete[] _array;
		// 更新对象成员
		_array = temp;
		_capacity *= 2;
	}

public:

	// 1.顺序表类的无参构造函数
	SeqList();            
	// 2.顺序表类的有参构造函数
	SeqList(DataType* array, size_t size);
	// 3.顺序表类的析构函数
	~SeqList();
	// 4.顺序表类的拷贝构造函数
	SeqList(const SeqList& s);

	// 5.顺序表类赋值运算符=的重载
	SeqList& operator=(const SeqList& s);
	// 6.打印顺序表
	void Print();

	// 7.尾插
	void PushBack(const DataType data);
	// 8.尾删
	void PopBack();
	
	// 9.头插
	void PushFront(DataType data);
	// 10.头删
	void PopFront();

	// 11.在顺序表的pos位置插入元素data
	// 注意：pos的范围必须在[0, ps->size]
	void Insert(int pos, DataType data);
	// 12.顺序表删除pos位置的值
	// 注意：pos的范围必须在[0, ps->size)
	void Erase(int pos);

	// 13.查找元素位置
	int SeqListFind(DataType data);

	// 14.检测顺序表是否为空
	bool isEmpty();

	// 15.获取顺序表中的第一个元素
	DataType SeqListFront();
	// 16.获取顺序表中最后一个元素
	DataType SeqListBack();
	// 17.获取顺序表中任意位置的元素
	DataType SeqListGet(int pos);

	// 18.清空顺序表
	void EmptySeqList();
	
	// 19.获取类成员size
	size_t Size()const;
	// 20.获取类成员capacity
	size_t Capacity()const;


private:
	// 定义一个动态顺序表
	DataType* _array; // 动态数组
	int _size;        // 有效元素的个数
	int _capacity;    // 表示空间总的大小
};

// 测试函数
void SeqList_test();