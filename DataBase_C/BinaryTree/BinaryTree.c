#include "BinaryTree.h"
#include "Queue.h"  //层序遍历需要借助队列结构
#include <assert.h>

// 0.构建二叉树结点
static BTNode* BuyBTNode(BTDataType d)
{
	// 在堆上开辟结点空间
	BTNode* newNode = (BTNode*)malloc(sizeof(BTNode));
	if (NULL == newNode)
	{
		assert(0);  //断言处理不合法情况
		return NULL;
	}
	// 赋值
	newNode->data = d;
	newNode->left = NULL;
	newNode->right = NULL;
	return newNode;
}

// 1.二叉树构建
// 通过前序遍历数组"ABD##E#H##CF##G##"构建 '#' 指空格
// 参数：array需要生成二叉树的数组；size数组大小；index构建的根结点下标
// 注意：
//		index作为递归结束标志，需传址传参
//		size作为数组长度，递归时长度不变
BTNode* BinaryTreeCreate(BTDataType* array, int size, int* index)
{
	BTNode* root = NULL;
	// 数据的合法性判断
	if (*index < size && array[*index] != ' ')
	{
		// 创建根节点
		root = BuyBTNode(array[*index]);
		// 递归创建左右子树
		++(*index);
		root->left = BinaryTreeCreate(array, size, index);
		++(*index);
		root->right = BinaryTreeCreate(array, size, index);
	}
	return root;
}

// 2.二叉树销毁
// 后序遍历销毁
// 注意二叉树传二级指针，因为修改了二叉树结构
void BinaryTreeDestory(BTNode** root)
{
	assert(root);
	// 如果当前根节点存在
	if (*root)
	{
		// 递归释放左右子树
		BinaryTreeDestory(&(*root)->left);
		BinaryTreeDestory(&(*root)->right);
		// 释放根结点
		free(*root);
		*root = NULL;
	}
}

// 3.二叉树前序遍历——递归遍历 
// 便利未修改二叉树结构，一级指针即可
void BinaryTreePrevOrder(BTNode* root)
{
	// 递归标志
	if (NULL == root)
	{
		return;
	}
	printf("%c ", root->data);
	BinaryTreePrevOrder(root->left);
	BinaryTreePrevOrder(root->right);
}
// 4.二叉树中序遍历——递归遍历
void BinaryTreeInOrder(BTNode* root)
{
	// 递归标志
	if (NULL == root)
	{
		return;
	}
	BinaryTreePrevOrder(root->left);
	printf("%c ", root->data);
	BinaryTreePrevOrder(root->right);
}
// 5.二叉树后序遍历——递归遍历
void BinaryTreePostOrder(BTNode* root)
{
	// 递归标志
	if (NULL == root)
	{
		return;
	}
	BinaryTreePrevOrder(root->left);
	BinaryTreePrevOrder(root->right);
	printf("%c ", root->data);
}
// 6.层序遍历——按层打印二叉树
void BinaryTreeLevelOrder(BTNode* root)
{
	// 判断root是否存在
	if (NULL == root)
	{
		return;
	}
	// 借助队列性质实现
	Queue q;  //定义队列（注意队列节点的类型一定是BTNode*）
	QueueInit(&q);  //初始化队列
	QueuePush(&q, root);  //将根节点入队
	while (!QueueEmpty(&q))  //当队列无结点时停止遍历
	{
		int sz = QueueSize(&q);
		char* arr = (char*)malloc(sz);  //保存当前层的所有结点
		for (int i = 0; i < sz; ++i)
		{
			BTNode* cur = QueueFront(&q);  //保存队列首位结点
			QueuePop(&q);  //队列首位结点出队
			arr[i] = cur->data;
			if (cur->left)  //如果左子树不为空
			{
				QueuePush(&q, cur->left);
			}
			if (cur->right)  //如果右子树不为空
			{
				QueuePush(&q, cur->right);
			}
		}
		// 打印该层结点
		for (int i = 0; i < sz; ++i)
		{
			printf("%c ", arr[i]);
		}
		printf("\n");
		free(arr);  //注意释放空间，避免内存泄露
	}
}

// 7.二叉树节点个数——递归查找
int BinaryTreeSize(BTNode* root)
{
	if (NULL == root)
	{
		return 0;
	}
	return 1 + BinaryTreeSize(root->left) + BinaryTreeSize(root->right);
}

// 8.二叉树叶子节点个数——递归查找
int BinaryTreeLeafSize(BTNode* root)
{
	if (NULL == root)
	{
		return 0;
	}
	if (root->left == NULL && root->right == NULL)
	{
		return 1;
	}
	return BinaryTreeLeafSize(root->left) + BinaryTreeLeafSize(root->right);
}

// 9.二叉树第k层节点个数——递归查找（默认根节点为第一层）
int BinaryTreeLevelKSize(BTNode* root, int k)
{
	if (NULL == root || k < 1)
	{
		return 0;
	}
	if (k == 1)
	{
		return 1;
	}
	return BinaryTreeLevelKSize(root->left, k - 1) + BinaryTreeLevelKSize(root->right, k - 1);
}

// 10.二叉树查找值为x的节点——递归查找
BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (NULL == root)
	{
		return NULL;
	}
	BTNode* ret = NULL;
	if (x == root->data)
	{
		return root;
	}
	if (ret = BinaryTreeFind(root->left, x))
	{
		return ret;
	}
	if (ret = BinaryTreeFind(root->right, x))
	{
		return ret;
	}
	return NULL;
}

// 11.判断二叉树是否是完全二叉树（0：不是；1：是）
int BinaryTreeComplete(BTNode* root)
{
	// 利用层序遍历思想，借助队列实现
	
	// 空树：返回1
	if (NULL == root)
	{
		return 1;
	}

	// 树不为空：将根节点入队列
	Queue q;
	QueueInit(&q);
	QueuePush(&q, root);

	// 并标记队头元素
	BTNode* cur = root;

	// 按层将二叉树结点存入队列
	// 按照层序遍历，找到第一个不饱和节点
	while ((cur = QueueFront(&q)) != NULL)
	{
		// 将队头元素出队列
		QueuePop(&q);
		QueuePush(&q, cur->left);
		QueuePush(&q, cur->right);
	}
	// 判断队列，若元素均为NULL，说明是完全二叉树；若存在有效元素，说明不是
	while (!QueueEmpty(&q))
	{
		cur = QueueFront(&q);
		if (cur != NULL)
		{
			return 0;
		}
		QueuePop(&q);
	}
	return 1;
}

// 测试函数
void BTtest()
{
	BTDataType array[] = "ABD  E H  CF  G  ";
	BTNode* root = NULL;  //创建根节点
	int index = 0;  //默认array数组0号下表为根
	root = BinaryTreeCreate(array, sizeof(array) / sizeof(array[0]), &index);

	// 前序遍历打印
	printf("前序遍历打印：\n");
	BinaryTreePrevOrder(root);
	printf("\n*************\n");

	// 中序遍历打印
	printf("中序遍历打印：\n");
	BinaryTreeInOrder(root);
	printf("\n*************\n");

	// 后序遍历打印
	printf("后序遍历打印：\n");
	BinaryTreePostOrder(root);
	printf("\n*************\n");

	// 层序遍历打印
	printf("层序遍历打印：\n");
	BinaryTreeLevelOrder(root);
	printf("*************\n");

	printf("二叉树结点个数为：%d\n", BinaryTreeSize(root));
	printf("*************\n");

	printf("二叉树叶子结点个数为：%d\n", BinaryTreeLeafSize(root));
	printf("*************\n");

	int k = 4;
	printf("二叉树第 %d 层的结点个数为 %d\n", k, BinaryTreeLevelKSize(root, k));
	printf("*************\n");

	if (BinaryTreeFind(root, 'D'))
	{
		printf("存在\n");
	}
	else
	{
		printf("不存在\n");
	}
	printf("*************\n");

	if (BinaryTreeComplete(root))
	{
		printf("该二叉树是完全二叉树！\n");
	}
	else
	{
		printf("该二叉树不是完全二叉树！\n");
	}
	printf("*************\n");
}