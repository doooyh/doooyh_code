#pragma once  //防止头文件重复调用

#include <stdio.h>
#include <windows.h>

// 二叉树的链式结构
/*
1.采用链表方式组成二叉树；

2.需注意二叉树的四种遍历方式：
    前序遍历：根左右
    中序遍历：左根右
    后序遍历：左右根
 	层序遍历：其实就是广度优先遍历
*/

// 定义二叉树结点
typedef char  BTDataType;
typedef struct BTNode
{
	BTDataType data;
	struct BTNode* left;
	struct BTNode* right;
}BTNode;

// 1.二叉树构建
// 通过前序遍历数组"ABD##E#H##CF##G##"构建
BTNode* BinaryTreeCreate(BTDataType* array, int size, int* index);

// 2.二叉树销毁
void BinaryTreeDestory(BTNode** root);

// 3.二叉树前序遍历 
void BinaryTreePrevOrder(BTNode* root);
// 4.二叉树中序遍历
void BinaryTreeInOrder(BTNode* root);
// 5.二叉树后序遍历
void BinaryTreePostOrder(BTNode* root);
// 6.层序遍历
void BinaryTreeLevelOrder(BTNode* root);

// 7.二叉树节点个数
int BinaryTreeSize(BTNode* root);
// 8.二叉树叶子节点个数
int BinaryTreeLeafSize(BTNode* root);
// 9.二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k);
// 10.二叉树查找值为x的节点
BTNode* BinaryTreeFind(BTNode* root, BTDataType x);

// 11.判断二叉树是否是完全二叉树
int BinaryTreeComplete(BTNode* root);

// 测试函数
void BTtest();