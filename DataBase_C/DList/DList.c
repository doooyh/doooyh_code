#include "DList.h"

////////////////////////////////////////////////////////////////////////

//动态申请一个结点空间
DLNode * buyDListNode(DataType data)
{
	DLNode * newNode = (DLNode*)malloc(sizeof(DLNode));
	if (NULL == newNode)
	{
		assert(0);
		return NULL;
	}
	newNode->prev = NULL;
	newNode->data = data;
	newNode->next = NULL;
	return newNode;
}

//创建链表 其实就是创建链表的头结点
//当循环链表只有头结点时，其指针指向自身，组成循环链表
DLNode * DListCreat()
{
	DLNode * head = buyDListNode(0);
	head->prev = head;
	head->next = head;
	return head;
}

//双向链表销毁
//链表销毁需要将头结点指向空，需修改头结点自身，故传二级指针
void DListDestory(DLNode** pplist)
{
	assert(pplist);
	DLNode* cur = (*pplist)->next;
	
	//采用头删法
	while (cur != *pplist)
	{
		//链表的销毁只要找到下一位结点遍历即可，无需处理结点的prev指针
		(*pplist)->next = cur->next;
		free(cur);
		cur = (*pplist)->next;
	}
	//当只剩一个结点时退出循环
	//释放头结点
	free(*pplist);
	*pplist = NULL;  //这里修改了指针内容 故需二级传参
}

//双向链表的打印
void DListPrint(DLNode* head)
{
	assert(head);
	DLNode* cur = head->next;
	while (cur != head)
	{
		printf("%d-->", cur->data);
		cur = cur->next;
	}
	printf("\n");
}

//双向链表查找
DLNode* DListFind(DLNode* head, DataType x)
{
	assert(head);
	DLNode* cur = head->next;
	while (cur != head)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}

//获取链表节点个数
int DListSize(DLNode* head)
{
	assert(head);
	DLNode* cur = head->next;
	int count = 0;
	while (cur != head)
	{
		count++;
		cur = cur->next;
	}
	return count;
}

//判断链表是否为空 （只有头结点）
int DListEmpty(DLNode* head)
{
	assert(head);
	return head->next == head;
}

//双向链表尾插 (通用，无论是否是只有头结点)
void DListPushBack(DLNode* head, DataType x)
{
	//assert(head);
	//DLNode* newNode = buyDListNode(x);

	//newNode->next = head;
	//newNode->prev = head->prev;
	//head->prev->next = newNode;
	//head->prev = newNode;

	//方法二：借用pos方法
	DListInsert(head, x);
}
//双向链表尾删 
void DListPopBack(DLNode* head)
{
	////若链表为空
	//if (DListEmpty(head))
	//{
	//	return;
	//}
	//DLNode* delNode = head->prev;

	//head->prev = delNode->prev;
	//delNode->prev->next = head;
	//free(delNode);
	//delNode = NULL;

	//方法二：借用pos方法
	DListErase(head->prev);
}

//双向链表头插 (通用，无论是否是只有头结点)
void DListPushFront(DLNode* head, DataType x)
{
	//assert(head);
	//DLNode* newNode = buyDListNode(x);
	////头结点后加新结点即可
	//newNode->next = head->next;
	//newNode->prev = head;
	//head->next = newNode;
	//newNode->next->prev = newNode;

	//方法二：借用pos方法
	DListInsert(head->next,x);
}
//双向链表头删
void DListPopFront(DLNode* head)
{
	////若链表为空
	//if (DListEmpty(head))
	//{
	//	return;
	//}
	//DLNode* delNode = head->next;

	//head->next = delNode->next;
	//delNode->next->prev = head;
	//free(delNode);
	//delNode = NULL;

	//方法二：借用pos方法
	DListErase(head->next);
}

//双向链表在pos的前面进行插入
void DListInsert(DLNode* pos, DataType x)
{
	if (NULL == pos)
	{
		return;
	}
	DLNode* newNode = buyDListNode(x);

	newNode->next = pos;
	newNode->prev = pos->prev;
	pos->prev->next = newNode;
	pos->prev = newNode;
}
//双向链表删除pos位置的节点
void DListErase(DLNode* pos)
{
	if (NULL == pos)
	{
		return;
	}
	pos->prev->next = pos->next;
	pos->next->prev = pos->prev;
	free(pos);
	pos = NULL;
}


/////////////////////////////////////////////////////////////////////////
static void Test1()
{
	DLNode * DListHead = DListCreat();
	DListPushBack(DListHead, 1);
	DListPushBack(DListHead, 2);
	DListPushFront(DListHead, 0);
	DListPushFront(DListHead, -1);

	DListPrint(DListHead);
	printf("%d\n", DListSize(DListHead));

	DListPopBack(DListHead);
	DListPopFront(DListHead);
	
	DListPrint(DListHead);
	printf("%d\n", DListSize(DListHead));



	DListDestory(&DListHead);
}
//双向链表测试函数
void TestDList()
{
	Test1();
}