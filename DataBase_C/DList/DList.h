#pragma once

#include<stdio.h>
#include<Windows.h>
#include<malloc.h>
#include<assert.h>
#pragma warning(disable:4996)

typedef int DataType;

typedef struct DListNode
{
	struct DListNode * prev;
	DataType data;
	struct DListNode * next;
}DLNode;

/////////////////////////////////////////////////////////////////////////
//ps:上次单链表采用的是无头结点，这次双向循环链表采用带头节点方式

//带头节点的优点在于一些功能函数实现时仅需传入头结点的一级指针即可
//因为头结点不是链表内容，不需要修改其内容




//带头节点双向循环链表的实现
//动态申请一个结点空间
DLNode * buyDListNode(DataType data);

//创建链表 返回一个指向链表结点的指针
DLNode * DListCreat();

//双向链表销毁
void DListDestory(DLNode** pplist);

//双向链表的打印
void DListPrint(DLNode* head);

//双向链表查找
DLNode* DListFind(DLNode* head, DataType x);

//获取链表节点个数
int DListSize(DLNode* head);

//判断链表是否为空
int DListEmpty(DLNode* head);

//双向链表尾插
void DListPushBack(DLNode* head, DataType x);
//双向链表尾删
void DListPopBack(DLNode* head);

//双向链表头插
void DListPushFront(DLNode* head, DataType x);
//双向链表头删
void DListPopFront(DLNode* head);

//双向链表在pos的前面进行插入
void DListInsert(DLNode* pos, DataType x);
//双向链表删除pos位置的节点
void DListErase(DLNode* pos);










//双向链表测试函数
void TestDList();