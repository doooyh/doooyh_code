#include "Heap.h"

//交换函数
static void Swap(HPDataType* left, HPDataType* right)
{
	HPDataType temp = *left;
	*left = *right;
	*right = temp;
}

//堆的向下排序算法（建堆、删堆元素、堆排序使用）
//对于一个完全二叉树从根节点向下遍历，形成大、小堆
 
//【注意】
// 1.使用堆的向下调整法时默认该完全二叉树的左右子树均为堆（只有叶节点也算堆）
// 2.向下调整算法的时间复杂度为该二叉树的高度O(log N)
// 3.这里涉及回调函数（回调比较方法函数 Less 或 Greater）

//参数：堆、堆的父母结点下标、大小堆标记（1为大堆，0为小堆）
void AdjustDown(Heap* hp, int parent)
{
	//建立一个标记 child
	//建大（小）堆时，child 用来指向两个孩子结点中较大（小）的结点
	//默认情况下 child 标记为左孩子，因为完全二叉树会存在只有左孩子的父母结点

	int child = parent * 2 + 1;  //父母结点从0取
	int size = hp->size;
	while (child < size)  //表明该 parent 左孩子在存在
	{
		//1.找需要与父节点比较的孩子结点（这里需要回调比较方法函数 Less 或 Greater） 
		if (child + 1 < size && hp->Compare(hp->array[child + 1], hp->array[child]))  //先判断是否有右孩子
		{
			child += 1;  //在建大（小）的比较中，右孩子更大（小），child 指向右孩子
		}
		//2.检测父结点parent是否满足要求
		if (hp->Compare(hp->array[child], hp->array[parent]))
		{
			//即父节点需要与孩子交换
			Swap(&hp->array[parent], &hp->array[child]);  //注意调用地址
			//向下层继续比较
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			//若父节点已满足要求说明向下排序完毕，直接退出
			return;
		}
	}
}

//堆的向上排序算法（堆插入使用）
void AdjustUp(Heap* hp)
{
	//从最后一个结点开始向上比较
	int child = hp->size - 1;
	int parent = (child - 1) / 2;  //child 默认为左孩子

	//这里无需挑选孩子结点，因为原有树已经符合堆规则，只有newNode可能不符合

	//循环比较，直至孩子结点均符合要求
	while (child > 0 && hp->Compare(hp->array[child], hp->array[parent])) //孩子结点不符合堆
	{
		Swap(&hp->array[parent], &hp->array[child]);
		//更新孩子、父亲结点
		child = parent;
		parent = (child - 1) / 2;
	}
	return;
}

// 判断堆空间
static void CheckCapacity(Heap* hp)
{
	assert(hp);
	if (hp->size == hp->capacity)
	{
		int newCapacity = hp->capacity * 2;  //空间翻倍
		// 1. 申请新空间
		int* temp = (HPDataType*)malloc(sizeof(HPDataType) * newCapacity);
		if (NULL == temp)
		{
			assert(0);
			return;
		}
		// 2. 拷贝元素
		for (int i = 0; i < hp->size; ++i)
		{
			*(temp + i) = hp->array[i];
		}
		// 3. 释放旧空间
		free(hp->array);

		hp->array = temp;
		hp->capacity = newCapacity;
	}
}

//两种比较方式函数——在向下排序的程序中会用到
//用来选择建小堆还是大堆
//建小堆
static int Less(HPDataType left, HPDataType right)
{
	return left < right;
}
//建大堆
static int Greater(HPDataType left, HPDataType right)
{
	return left > right;
}

//-----------------------------------------------------------------------------------------------------------------

// 堆的构建
//【注意】
// 1.建堆的前提是完全二叉树
// 2.建堆与向下排序算法搭配，大约有N/2个结点需要向下排序
// 3.向下排序时间复杂度为O(log N)，所以建堆时间复杂度为 N/2 * log N
// 4.但是！！！建堆的时间复杂度化简后为 O（N），而不是O（N*log N）
// 5.第四个参数这里用到了回调函数的知识点！！！
// 6.为什么不采用向上排序？由于堆构建的二叉树完全是乱序的，采用向上排序每轮排序结果会相互影响，造成浪费

//参数：堆的地址、带建堆的二叉树元素数组、元素个数、指向比较方式函数的指针
void hpCreat(Heap* hp, HPDataType a[], int n, COM com)
{
	//1.初始化堆
	//开辟空间
	hp->array = (HPDataType*)malloc(sizeof(HPDataType) * n);
	if (NULL == hp->array)
	{
		assert(0);
		return;
	}
	//元素赋值
	hp->capacity = n;
	hp->size = 0;

	//2.将二叉树数组元素拷贝至堆中——这里涉及堆的向下排序
	memcpy(hp->array, a, sizeof(HPDataType) * n);
	hp->size = n;
	hp->Compare = com;

	//3.向下排序调整堆(从最后一个非叶子节点开始，即最后一个结点的父节点)
	//在完全二叉树中，第i个结点的左孩子：2i+1、右孩子：2i+2 
	//所以在一个有 n 个结点的完全二叉树中，最后一个非叶子节点为 (n - 2) / 2
	for (int root = (n - 2) / 2; root >= 0; root--)
	{
		AdjustDown(hp, root);
	}
}

// 堆的销毁
void HeapDestory(Heap* hp)
{
	assert(hp);
	if (hp->array)
	{
		free(hp->array);
		hp->array = NULL;
		hp->size = 0;
		hp->capacity = 0;
		hp->Compare = NULL;
	}
}

// 堆的插入——采用向上排序算法
//末尾插入新元素，那么有可能会使堆结构不成立
//从插入的元素开始向上重新比较，使用一次向上排序即可
void HeapPush(Heap* hp, HPDataType x)
{
	assert(hp);
	//1.先判断堆结构是否还有多余空间
	CheckCapacity(hp);

	//2.插入新元素
	hp->array[hp->size] = x;
	hp->size++;
	
	//3.向上排序
	AdjustUp(hp);
}

// 堆的删除（删堆顶）
//删除根节点，故采用了向下排序法
//时间复杂度 = 一次向下排序 = O（log N）
void HeapPop(Heap* hp)
{
	//1.判断堆中是否有元素
	if (HeapEmpty(hp))
	{
		printf("堆已为空！");
		return;
	}
	//2.交换堆顶元素与队尾元素（方便向下排序）
	Swap(&hp->array[hp->size - 1], &hp->array[0]);
	hp->size--;

	//3.重新向下排序
	AdjustDown(hp, 0);  
	//从尾节点移到根节点位置，原有堆破坏
	//但不影响原来的左右孩子子树亦然成堆，故从根节点重新向下排序
}

// 取堆顶的数据
HPDataType HeapTop(Heap* hp)
{
	//判断堆中是否有元素
	if (HeapEmpty(hp))
	{
		printf("堆已为空！");
		return -1;
	}
	return hp->array[0];
}

// 堆的数据个数
int hpSize(Heap* hp)
{
	assert(hp);
	return hp->size;
}

// 堆的判空
int HeapEmpty(Heap* hp)
{
	assert(hp);
	return hp->size == 0;
}

//-----------------------------------------------------------------------------------------------------------------

static void test1()
{
	HPDataType arr[] = { 6, 1, 8, 2, 7, 3, 9, 4, 5 };

	Heap obj;
	int size = sizeof(arr) / sizeof(arr[0]);
	hpCreat(&obj, arr, size, Less);  //引用Less比较方法建小堆
	printf("堆顶：%d\n", HeapTop(&obj));  //输出堆顶元素
	printf("堆元素：%d\n", hpSize(&obj));  //输出元素个数

	HeapPush(&obj, 0);  //插入新元素至堆顶
	printf("插入0后堆顶：%d\n", HeapTop(&obj));  //输出堆顶元素
	printf("堆元素：%d\n", hpSize(&obj));  //输出元素个数

	HeapPop(&obj);  //删除堆顶元素
	printf("一次删除后堆顶：%d\n", HeapTop(&obj));  //输出堆顶元素
	printf("堆元素：%d\n", hpSize(&obj));  //输出元素个数

	HeapDestory(&obj);  //销毁
}

// 测试函数
void TestHeap()
{
	test1();
}