#pragma once  //防止头文件重复调用

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <assert.h>
#include <Windows.h>



//堆 一种用来存储完全二叉树的特殊结构
//有大（根）堆、小（根）堆之分
//一般通过顺序表结构实现
//采取动态申请顺序表


//堆元素类型
typedef int HPDataType;  

// 补：定义一个函数指针变量，该函数返回值类型为int，有两个int参数
// 即 一个指向函数的指针
// 1.先声明一个函数：
// int Compare(int left, int right);
 
// 2.加上 * 即为函数指针（注意* 与函数名在一起）
// int (*Compare)(int left, int right);

// 3.加上typedef后，原变量名的位置变为了该函数指针类型重定义的类型名
typedef int (*COM)(int, int);  //这是一个名为Compare的函数指针类型
// 该函数指针类型在堆结构里加入


//堆结构定义（与顺序表一样）
typedef struct Heap
{
	HPDataType* array;  //储存在堆（这个堆为内存上存放动态开辟数据的空间）上的数组
	int size;  //有效元素个数
	int capacity;  //开辟堆空间大小
	COM Compare;  //定义一个函数指针 用来调用：两种不同的比较方式函数（因为建立大小堆的比较方式不同）
}Heap;

static int Less(HPDataType left, HPDataType right);

static int Greater(HPDataType left, HPDataType right);

//-----------------------------------------------------------------------------------------------------------------

// 堆的构建
void hpCreat(Heap* hp, HPDataType a[], int n, COM com);
// 堆的销毁
void HeapDestory(Heap* hp);
// 堆的插入
void HeapPush(Heap* hp, HPDataType x);
// 堆的删除
void HeapPop(Heap* hp);
// 取堆顶的数据
HPDataType HeapTop(Heap* hp);
// 堆的数据个数
int hpSize(Heap* hp);
// 堆的判空
int HeapEmpty(Heap* hp);

//-----------------------------------------------------------------------------------------------------------------

// 测试函数
void TestHeap();
