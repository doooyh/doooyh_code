#include "HeapSort.h"
#include "Heap.h"

// 该代码为堆的应用——堆排序和TOP-k问题


//-----------------------------------------------------------------------------------------------------------------

// 1.堆排序——时间复杂度O（N * logN）
// 堆排序利用的是堆删除思想进行排序（那么就要使用向下排序算法）
// 堆删除本质是将堆顶元素与最后元素进行交换，在将数组的最后元素（即为堆顶元素）忽视
// 那么堆排序利用向下排序，“删除”所有元素后即可得到有序的数组（循环删除操作）
// 若需升序排序，我们需要让堆顶最大，那么就需要建立大堆
// 反之降序排序，就需要建立小堆

void HeapAdjust(int arr[], int size, int parent, int(*cmp)(int, int))
{
	int child = parent * 2 + 1;

	while (child < size)
	{
		if (child + 1 < size && cmp(arr[child + 1], arr[child]))  //确定需交换的孩子
		{
			child++;
		}
		if (cmp(arr[child], arr[parent]))  //确定父节点是否需要交换
		{
			int temp = arr[child];
			arr[child] = arr[parent];
			arr[parent] = temp;

			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			return;
		}
	}
}

// 【注意】堆排序采用的是堆的思想，并非一定要建堆结构
void hpSort(int arr[], int size, int(*cmp)(int, int))
{
	// 建堆
	// 升序：大堆 降序：小堆
	for (int root = (size - 2) / 2; root >= 0; root--)
	{
		HeapAdjust(arr, size, root, cmp);
	}
	// 利用删除思想排序
	// 有size个结点，则需排size-1遍
	int cur = size;
	while (cur--)
	{
		// 交换首尾元素
		int temp = arr[0];
		arr[0] = arr[cur];
		arr[cur] = temp;

		// 重新建立堆（借用向下排序）
		HeapAdjust(arr, cur, 0, cmp);
	}
}

// 测试函数
void TestHeapSort()
{
	int array[] = { 6, 1, 8, 0, 2, 7, 3, 9, 4, 5 };

	hpSort(array, sizeof(array) / sizeof(array[0]), Greater);  // Greater回调函数 建立升序

	for (int i = 0; i < sizeof(array) / sizeof(array[0]); i++)
	{
		printf("%d ", array[i]);
	}
	printf("\n");
}

void TestHeap2()
{
	TestHeapSort();
}

//-----------------------------------------------------------------------------------------------------------------

// 2.TOP-K问题——外部排序典范
// 【问题】在数据量比较大的情况下，求一组数据中前K个最大（小）的元素（k一般远小于元素个数）
// 【方法】用数据前K个元素来建堆，再利用剩余N-K个元素与堆顶元素依次进行比较，不满足的与堆顶替换即可
// 【分析】找前k个最大的，建小堆；反之建大堆
// 这是一种典型的外部排序，当数据量极大时，一次将所有待排序元素读取至排序函数有可能会引起栈溢出
// 那么利用堆来进行外部排序，循环比较每次只排部分元素
// 【注意】时间复杂度：k + (N - k)*log k(k很小)

void hpTopK(int arr[], int size, int k, int(*cmp)(int, int))
{
	if (k > size || k < 0)
	{
		printf("数据错误！排序失败");
		return;
	}
	// 1.将数据前k个元素，建立一个大小为k的堆
	Heap hpTop;
	hpCreat(&hpTop, arr, k, cmp);  // 例：找前k个最大值。建小堆，cmp = Less

	// 2.循环比较后size-k个元素与堆顶的大小关系
	for (int i = k; i < size; i++)
	{
		if (cmp(HeapTop(&hpTop), arr[i]))  // 若堆中最小的堆顶比后面的元素小，就需要重新组堆
		{
			// 堆顶出堆
			HeapPop(&hpTop);
			// 后面元素入堆（入堆函数会自动排序组成新堆）
			HeapPush(&hpTop, arr[i]);
		}
	}

	// 3.打印
	
	/*
	// 【方法一】在Top-K问题中，原数组并没有直接修改
	for (int i = 0; i < k; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
	*/
	
	/*
	// 【方法二】直接输出堆内的数组（可是并不一定是有序）
	for (int i = 0; i < k; i++)
	{
		printf("%d ", hpTop.array[i]);
	}
	printf("\n");*/

	// 【方法三】按出堆方式，每次只输出堆顶，这样有序
	for (int i = 0; i < k; ++i) 
	{
		printf("%d ", HeapTop(&hpTop));
		HeapPop(&hpTop);
	}

	// 4.销毁堆
	HeapDestory(&hpTop);
}


// 测试函数
void TesthpTopK()
{
	int n = 10000;
	int* a = (int*)malloc(sizeof(int) * n);
	if (NULL == a)
	{
		assert(0);
	}

	srand((unsigned)time(NULL));  //设置随机种子
	//随机生成10000个数存入数组，保证元素都小于1000000
	for (int i = 0; i < n; ++i)
	{
		a[i] = rand() % 1000000;
	}
	//确定10个最大的数
	a[5] = 1000000 + 1;
	a[1231] = 1000000 + 2;
	a[531] = 1000000 + 3;
	a[5121] = 1000000 + 4;
	a[115] = 1000000 + 5;
	a[2335] = 1000000 + 6;
	a[9999] = 1000000 + 7;
	a[76] = 1000000 + 8;
	a[423] = 1000000 + 9;
	a[3144] = 1000000 + 10;

	hpTopK(a, n, 10, Less);  //使用回调函数Less 建小堆；找前k个最大的
}

void TestHeap3()
{
	TesthpTopK();
}











