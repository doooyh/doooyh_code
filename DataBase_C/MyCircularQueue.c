//力扣622. 设计循环队列
//https://leetcode-cn.com/problems/design-circular-queue/

#include<stdio.h>
#include<assert.h>
#include<stdbool.h>
#include<Windows.h>

//循环队列以定长数组构成的顺序表为基础
//注意队列特性 前出后进（头删与尾插）

//循环队列结构
typedef struct
{
    int* array; // 指向存储元素空间的起始位置
    int front;  // 队首元素
    int rear;   // 队尾元素
    int size;   // 队列可存放元素
} MyCircularQueue;

//初始化队列
MyCircularQueue* myCircularQueueCreate(int k)
{
    if (k < 0)
    {
        return NULL;
    }
        
    MyCircularQueue* mcq = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
    if (NULL == mcq)
    {
        assert(0);
        return NULL;
    }
    //循环队列实际申请空间是他的容量+1
    //这样可以区分空队列与满队列
    mcq->array = (int*)malloc(sizeof(int) * (k + 1));
    mcq->front = 0;
    mcq->rear = 0;
    mcq->size = k;

    return mcq;
}

//判断循环队列是否已满
bool myCircularQueueIsFull(MyCircularQueue* obj)
{
    assert(obj);
    //当循环队列满时，头尾指针相邻
    if ((obj->rear + 1) % (obj->size + 1) == (obj->front))
    {
        return true;
    }
    return false;
}

//判断循环队列是否为空
bool myCircularQueueIsEmpty(MyCircularQueue* obj)
{
    assert(obj);
    //当循环队列为空时，头尾指针指向一个
    return obj->front == obj->rear;
}

//向循环队列插入一个元素
//如果成功插入则返回
bool myCircularQueueEnQueue(MyCircularQueue* obj, int value)
{
    assert(obj);
    //1.判断循环队列是否已满
    if (myCircularQueueIsFull(obj))
    {
        return false;
    }
    //2.插入新元素（尾插）
    obj->array[obj->rear] = value;
    //3.更新尾指针
    obj->rear = (obj->rear + 1) % (obj->size + 1);
    //4.插入成功
    return true;
}

//删除循环队列元素（头删）
//如果成功插入则返回
bool myCircularQueueDeQueue(MyCircularQueue* obj)
{
    assert(obj);
    //1.判断循环队列是否为空
    if (myCircularQueueIsEmpty(obj))
    {
        return false;
    }
    //2.更新头指针即可
    obj->front = (obj->front + 1) % (obj->size + 1);
    //3.删除成功
    return true;
}


//返回队首元素
int myCircularQueueFront(MyCircularQueue* obj)
{
    assert(obj);
    //若队列为空
    if (myCircularQueueIsEmpty(obj))
    {
        return -1;
    }
    return obj->array[obj->front];
}

//返回队尾元素
int myCircularQueueRear(MyCircularQueue* obj)
{
    assert(obj);
    //若队列为空
    if (myCircularQueueIsEmpty(obj))
    {
        return -1;
    }
    return obj->array[(obj->rear-1 + obj->size+1)%(obj->size+1)];  //不用(obj->rear-1)是防止(obj->rear-1)为负数
}

//释放循环队列
void myCircularQueueFree(MyCircularQueue* obj)
{
    assert(obj);
    free(obj->array);
    free(obj);
}

void Test1()
{
    MyCircularQueue* cq = myCircularQueueCreate(3);
    if (myCircularQueueIsEmpty(cq))  //判空
    {
        printf("队列为空\n");
    }
    else
    {
        printf("队列不为空\n");
    }

    myCircularQueueEnQueue(cq, 1);  //插入
    myCircularQueueEnQueue(cq, 2);
    myCircularQueueEnQueue(cq, 3);

    if (myCircularQueueIsEmpty(cq))  //判空
    {
        printf("队列为空\n");
    }
    else
    {
        printf("队列不为空\n");
    }

    if (myCircularQueueIsFull(cq))  //判满
    {
        printf("队列为满\n");
    }
    else
    {
        printf("队列不满\n");
    }

    printf("队首：%d\n", myCircularQueueFront(cq));
    printf("队尾：%d\n", myCircularQueueRear(cq));

    myCircularQueueDeQueue(cq);  //删除

    printf("队首：%d\n", myCircularQueueFront(cq));
    printf("队尾：%d\n", myCircularQueueRear(cq));

    myCircularQueueFree(cq);
}

int main()
{
    Test1();

    system("pause");
    return 0;
}
