#include "Queue.h"


QNode* buyQNode(QDataType data)
{
	QNode* newQNode = (QNode*)malloc(sizeof(QNode));
	if (NULL == newQNode)
	{
		assert(0);
		return NULL;
	}
	newQNode->data = data;
	newQNode->next = NULL;

	return newQNode;
}

///////////////////////////////////////////////////////////

// 初始化队列
void QueueInit(Queue* q)
{
	assert(q);
	q->front = NULL;
	q->rear = NULL;
	q->size = 0;
}

// 销毁队列
void QueueDestroy(Queue* q)
{
	assert(q);
	QNode* cur = q->front;
	while (cur)
	{
		q->front = cur->next;
		free(cur);
		cur = q->front;
	}
	q->front = NULL;
	q->rear = NULL;
	q->size = 0;
}

// 检测队列是否为空
int QueueEmpty(Queue* q)
{
	assert(q);
	return 0 == q->size;
}

// 入队列 尾插
void QueuePush(Queue* q, QDataType data)
{
	assert(q);
	QNode* newQNode = buyQNode(data);
	//1.队列为空
	if (QueueEmpty(q))
	{
		q->front = newQNode;
	}
	else
	{
		//2.队列已有元素
		q->rear->next = newQNode;
	}
	//3.调整队列其余元素
	q->rear = newQNode;
	q->size++;

	return;
}

// 出队列 头删
void QueuePop(Queue* q)
{
	assert(q);
	//1.队列为空
	if (QueueEmpty(q))
	{
		assert(0);
		return;
	}
	else
	{
		//2.队列已有元素
		QNode* delNode = q->front;
		q->front = q->front->next;
		q->size--;
		free(delNode);
		if (NULL == q->front)
		{
			q->rear = NULL;
		}
	}
}

// 获取队头元素
QDataType QueueFront(Queue* q)
{
	//队列为空情况设为违法
	assert(!QueueEmpty(q));
	return q->front->data;
}

// 获取队尾元素
QDataType QueueBack(Queue* q)
{
	//队列为空情况设为违法
	assert(!QueueEmpty(q));
	return q->rear->data;
}

// 获取队列中有效元素个数
int QueueSize(Queue* q)
{
	assert(q);
	return q->size;
}



/////////////////////////////////////////////////////////////
static test1()
{
	Queue q;
	QueueInit(&q);

	QueuePush(&q, 1);
	QueuePush(&q, 2);
	QueuePush(&q, 3);
	printf("队头：%d\n", QueueFront(&q));
	printf("队尾：%d\n", QueueBack(&q));
	printf("队列元素个数：%d\n", QueueSize(&q));

	QueuePop(&q);
	printf("队头：%d\n", QueueFront(&q));
	printf("队尾：%d\n", QueueBack(&q));
	printf("队列元素个数：%d\n", QueueSize(&q));

	//销毁 不然有可能内存泄漏
	QueueDestroy(&q);
}

void TestQueue()
{
	test1();
}