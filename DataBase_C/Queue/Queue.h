#pragma once

#include<stdio.h>
#include<Windows.h>
#include<assert.h>
#include<malloc.h>
#pragma warning(disable:4996)

// 队列是一种前出后进的线性表，可以通过顺序表和链表实现（一般使用单链表）
// 若使用顺序表，头删时时间复杂度太高
// 从队尾入队列 从队头出队列

typedef int QDataType;


// 定义单链表结点
typedef struct QNode
{
	QDataType data;
	struct QNode* next;
}QNode;

// 定义队列结构体
typedef struct Queue
{
	QNode* front;  //指向队头结点 队头：出队
	QNode* rear;   //指向队尾结点 队尾：入队
	int size;      //队列元素个数
}Queue;

////////////////////////////////////////////////////////////////

// 初始化队列
void QueueInit(Queue* q);

// 销毁队列
void QueueDestroy(Queue* q);

// 入队列
void QueuePush(Queue* q, QDataType data);

// 出队列
void QueuePop(Queue* q);

// 获取队头元素
QDataType QueueFront(Queue* q);

// 获取队尾元素
QDataType QueueBack(Queue* q);

// 获取队列中有效元素个数
int QueueSize(Queue* q);

// 检测队列是否为空
int QueueEmpty(Queue* q);


/////////////////////////////////////////////////////////////
void TestQueue();