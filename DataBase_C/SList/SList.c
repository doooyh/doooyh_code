#include "SList.h"

/////////////////////////////////////////////////////////
//动态申请一个元素空间
SLNode* BuySListNode(DataType x)
{
	SLNode* newNode = (SLNode*)malloc(sizeof(SLNode));
	if (NULL == newNode)
	{
		assert(0);
		return NULL;
	}
	newNode->data = x;
	newNode->next = NULL;

	return newNode;
}

// 单链表尾插  空链表尾插O(1)、一段链表尾插O(N)
void SListPushBack(SLNode** pplist, DataType x)
{
	assert(pplist); //指向链表的指针是否存在
	
	//1.链表为空
	if (NULL == *pplist)
	{
		*pplist = BuySListNode(x);
		return;
	}
	else  //2.链表不为空
	{
		//a.找当前链表最后一个节点
		SLNode* cur = *pplist;
		while (cur->next != NULL)
		{
			cur = cur->next;
		}
		//b.插入新元素
		cur->next = BuySListNode(x);
	}
}
// 单链表的头插 时间复杂度O(1)
void SListPushFront(SLNode** pplist, DataType x)
{
	/*
	assert(pplist);
	SLNode* newNode = BuySListNode(x);
	if (NULL == *pplist){
		*pplist = newNode;
	}
	else
	{
		newNode->next = *pplist;
		*pplist = newNode;
	}
	*/
	assert(pplist);
	//无论链表是否有值，头插法均可直接插值 因为若为空，*pplist == NULL
	SLNode *newNode = BuySListNode(x);
	newNode->next = *pplist;
	*pplist = newNode;
}

// 单链表的尾删
void SListPopBack(SLNode** pplist)
{
	assert(pplist);
	//1.链表为空
	if (NULL == *pplist)
	{
		printf("链表为空\n");
		return;
	}
	else if (NULL == (*pplist)->next) //2.链表只有一个结点
	{
		free(*pplist);
		*pplist = NULL;
	}
	else //3.链表有多个结点
	{
		//a.找到最后结点，并为前一个结点留一个标记指针
		SLNode* cur = *pplist;
		SLNode* pre = NULL;
		while (cur->next != NULL)
		{
			pre = cur;
			cur = cur->next;
		}
		//b.删除结点
		pre->next = NULL;
		free(cur);
	}
}
// 单链表头删 时间复杂度O(1)
void SListPopFront(SLNode** pplist)
{
	assert(pplist);
	//1.空链表直接退出
	if (NULL == *pplist)
	{
		printf("链表为空\n");
		return;
	}
	else //2.链表非空
	{
		SLNode* delNode = *pplist;
		*pplist = delNode->next;
		free(delNode);
	}
}

// 单链表查找
SLNode* SListFind(SLNode* plist, DataType x)
{
	SLNode* cur = plist;
	while (cur)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}

// 单链表在pos位置之后插入x
// 分析思考为什么不在pos位置之前插入？
void SListInsertAfter(SLNode* pos, DataType x)
{
	if (NULL == pos)
	{
		printf("插入点异常\n");
		return;
	}
	SLNode* newNode = BuySListNode(x);
	newNode->next = pos->next;
	pos->next = newNode;
}
// 单链表删除pos位置之后的值
// 分析思考为什么不删除pos位置？
void SListEraseAfter(SLNode* pos)
{
	if (NULL == pos)
	{
		printf("删除点异常\n");
		return;
	}
	SLNode* delNode = pos->next;
	pos->next = delNode->next;
	free(delNode);
}

// 释放链表
void SListDestroy(SLNode** pplist)
{
	assert(pplist);
	SLNode* cur = *pplist;
	while (cur)
	{
		*pplist = cur->next;
		free(cur);
		cur = *pplist;
	}
	*pplist = NULL;
}

// 单链表打印
void SListPrint(SLNode* plist)
{
	SLNode* cur = plist;
	while (cur)
	{
		printf("%d--->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}
// 单链表逆序打印----递归方法
void SListReversePrint(SLNode* plist)
{
	if (NULL != plist)
	{
		SListPrint(plist->next);
		printf("%d-->", plist->data);
	}
}


static void Test1()
{
	SLNode* pList = NULL;
	SListPushBack(&pList, 1);
	SListPushBack(&pList, 2);
	SListPushFront(&pList, 0);
	SListPushFront(&pList, -1);
	SListPrint(pList);

	SListPopBack(&pList);
	SListPopFront(&pList);
	SListPrint(pList);

	printf("%s\n", (char*)SListFind(pList, -1));
}

//链表测试函数
void TestSList()
{
	Test1();
}