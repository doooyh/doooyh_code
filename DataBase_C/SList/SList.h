#pragma once

#include<stdio.h>
#include<Windows.h>
#include<malloc.h>
#include<assert.h>
#pragma warning(disable:4996)

typedef int DataType;



//链表结构体有两种：有头结点 / 无头结点
typedef struct SListNode
{
	DataType data;
	struct SListNode* next;
}SLNode;

////////////////////////////////////////////////////////////////
//无头+单向+非循环链表的函数实现
// 
// 动态申请一个节点：链表不是连续空间，故以元素为单位开辟空间
SLNode* BuySListNode(DataType x);

// 单链表打印
void SListPrint(SLNode* plist);
// 单链表逆序打印----递归方法
void SListReversePrint(SLNode* plist);

// 单链表尾插
void SListPushBack(SLNode** pplist, DataType x);

// 单链表的头插
void SListPushFront(SLNode** pplist, DataType x);

// 单链表的尾删
void SListPopBack(SLNode** pplist);

// 单链表头删
void SListPopFront(SLNode** pplist);

// 单链表查找
SLNode* SListFind(SLNode* plist, DataType x);

// 单链表在pos位置之后插入x
// 分析思考为什么不在pos位置之前插入？
void SListInsertAfter(SLNode* pos, DataType x);

// 单链表删除pos位置之后的值
// 分析思考为什么不删除pos位置？
void SListEraseAfter(SLNode* pos);

// 释放链表
void SListDestroy(SLNode** pplist);


//////////////////////////////////////////////////////////
//链表测试函数
void TestSList();

