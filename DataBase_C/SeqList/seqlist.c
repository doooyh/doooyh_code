#include "seqlist.h"


// 检测链表空间
static void ChecKCapacity(SeqList* ps)
{
	assert(ps);
	if (ps->size == ps->capacity)
	{
		//开辟新空间
		int newCapacity = (ps->capacity << 1); //左移一位就是两倍大小
		DataType* temp = (DataType*)malloc(newCapacity * sizeof(DataType));
		if (NULL == temp)
		{
			assert(0);
			return;
		}
		//原空间值拷贝到新空间
		for (int i = 0; i < ps->size; i++)
		{
			temp[i] = ps->array[i];
		}
		//释放原堆空间
		free(ps->array);

		ps->array = temp;
		ps->capacity = newCapacity;
	}
}

// 打印函数
static void SeqListPrint(SeqList* ps)
{
	assert(ps);
	for (int i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->array[i]);
	}
	printf("\n");
}

///////////////////////////////////////////////////////////////////
// 1.初始化动态顺序表
void SeqListInit(SeqList* ps, int initCapacity)
{
	assert(ps); //检测传入地址是否合法
	initCapacity = initCapacity <= 0 ? 3 : initCapacity;  //检测第二个参数 若非法默认开辟三个空间
	ps->array = (DataType*)malloc(initCapacity * sizeof(DataType));
	if (NULL == ps->array)
	{
		assert(0); //通过assert显示错误原因
		return;
	}

	ps->capacity = initCapacity;
	ps->size = 0;
}

// 2.释放链表
void SeqListDestroy(SeqList* ps)
{
	assert(ps);
	if (ps->array)
	{
		free(ps->array); //链表内存上连续开辟，整体释放
		ps->array = NULL;
		ps->capacity = 0;
		ps->size = 0;
	}
}

// 3.尾插
void SeqListPushBack(SeqList* ps, DataType data)
{
	assert(ps);
	//1.检测空间是否足够
	ChecKCapacity(ps);

	//2.最后一个元素插入data
	ps->array[ps->size] = data;
	ps->size++;
}
// 4.尾删
void SeqListPopBack(SeqList* ps)
{
	assert(ps);
	//1.检测链表是否有元素
	if (SeqListEmpty(ps))
	{
		printf("ERROR:顺序表无内容\n");
		return;
	}
	//2.删除
	ps->size--; //只改变合法元素个数即可，无需将空间释放
}

// 5.头插
void SeqListPushFront(SeqList* ps, DataType data)
{
	assert(ps);
	//1.检测空间是否足够
	ChecKCapacity(ps);

	//2.将所有元素整体向后移一位
	for (int i = ps->size - 1; i >= 0; i--)
	{
		ps->array[i + 1] = ps->array[i];
	}
	//3.插入元素
	ps->array[0] = data;
	ps->size++;
}
// 6.头删
void SeqListPopFront(SeqList* ps)
{
	assert(ps);
	//1.检测链表是否有元素
	if (SeqListEmpty(ps))
	{
		printf("ERROR:顺序表无内容\n");
		return;
	}
	//2.将所有元素整体向前移一位
	for (int i = 0; i < ps->size; i++)
	{
		ps->array[i] = ps->array[i + 1];
	}
	ps->size--;
}

// 7.在顺序表的pos位置插入元素data
// 注意：pos的范围必须在[0, ps->size]
void SeqListInsert(SeqList* ps, int pos, DataType data) //pos为下标从0开始
{
	assert(ps);
	//1.位置检测
	if (pos<0 || pos>ps->size)
	{
		printf("ERROR:插入位置超出范围\n");
		return;
	}
	//2.容量检测
	ChecKCapacity(ps);
	//3.pos后元素后移一位
	for (int i = ps->size; i >= pos; i--)
	{
		ps->array[i + 1] = ps->array[i];
	}
	//4.插入元素
	ps->array[pos] = data;
	ps->size++;
}

// 8.顺序表删除pos位置的值
// 注意：pos的范围必须在[0, ps->size)
void SeqListErase(SeqList* ps, int pos)  //pos为下标从0开始
{
	assert(ps);
	//1.位置检测
	if (pos<0 || pos>ps->size)
	{
		printf("ERROR:插入位置超出范围\n");
		return;
	}
	//2.pos和其后元素向前移一位
	for (int i = pos; i < ps->size; i++)
	{
		ps->array[i] = ps->array[i + 1];
	}
	ps->size--;
}

// 9.有效元素个数
int SeqListSize(SeqList* ps)
{
	assert(ps);
	return ps->size;
}

// 10.空间总的大小
int SeqListCapacity(SeqList* ps)
{
	assert(ps);
	return ps->capacity;
}

// 11.查找元素位置
int SeqListFind(SeqList* ps, DataType data)
{
	assert(ps);
	for (int i = 0; i < ps->size; i++)
	{
		if (data == ps->array[i])
		{
			return i;
		}
	}
	return -1;
}

// 12.检测顺序表是否为空
int SeqListEmpty(SeqList* ps)
{
	assert(ps);
	return 0 == ps->size;  //若为空，等式为真，返回值为1
}

// 13.获取顺序表中的第一个元素
DataType SeqListFront(SeqList* ps)
{
	assert(ps);
	return ps->array[0];
}

// 14.获取顺序表中最后一个元素
DataType SeqListBack(SeqList* ps)
{
	assert(ps);
	return ps->array[ps->size];
}

// 15.获取顺序表中任意位置的元素
DataType SeqListGet(SeqList* ps, int pos)
{
	assert(ps);
	return ps->array[pos];
}
///////////////////////////////////////////////////////////

//测试函数1
static void Test1()
{
	SeqList s;
	SeqListInit(&s, 3); //注意这里参数传的是地址

	SeqListPushBack(&s, 1);
	SeqListPushBack(&s, 2);
	SeqListPushBack(&s, 3);
	SeqListPushBack(&s, 4);
	SeqListPushBack(&s, 5);
	SeqListPushBack(&s, 6);
	SeqListPrint(&s);

	SeqListPushFront(&s, 0);
	SeqListPrint(&s);

	printf("顺序表有效数字：%d\n", SeqListSize(&s));
	printf("顺序表开辟空间：%d\n", SeqListCapacity(&s));
	printf("顺序表中数字5的下标：%d\n", SeqListFind(&s, 5));

	SeqListDestroy(&s);
}

//测试顺序表
void TestSeqList()
{
	Test1();
}