//头文件 用来声明调用函数
#pragma once

#include<stdio.h>
#include<Windows.h>
#include<malloc.h>
#include<assert.h>
#pragma warning(disable:4996)

/*
// 1.静态顺序表：顺序表中的元素存储在一段固定的数组中
#define MAXSIZE 100

struct SeqList
{
	int array[MAXSIZE];
	int size;
};
*/

// 2.动态顺序表: 底层的空间从堆上动态申请出来的
typedef int DataType;

typedef struct SeqList
{
	DataType* array;     // 指向存储元素空间的起始位置
	int capacity;   // 表示空间总的大小
	int size;       // 有效元素的个数
}SeqList;


// typedef struct SeqList   SeqList;

//////////////////////////////////////////////////////////// 
// 1.初始化动态顺序表
void SeqListInit(SeqList* ps, int initCapacity);

// 2.释放链表
void SeqListDestroy(SeqList* ps);

// 3.尾插
void SeqListPushBack(SeqList* ps, DataType data);
// 4.尾删
void SeqListPopBack(SeqList* ps);

// 5.头插
void SeqListPushFront(SeqList* ps, DataType data);
// 6.头删
void SeqListPopFront(SeqList* ps);

// 7.在顺序表的pos位置插入元素data
// 注意：pos的范围必须在[0, ps->size]
void SeqListInsert(SeqList* ps, int pos, DataType data);

// 8.顺序表删除pos位置的值
// 注意：pos的范围必须在[0, ps->size)
void SeqListErase(SeqList* ps, int pos);

// 9.有效元素个数
int SeqListSize(SeqList* ps);

// 10.空间总的大小
int SeqListCapacity(SeqList* ps);

// 11.查找元素位置
int SeqListFind(SeqList* ps, DataType data);

// 12.检测顺序表是否为空
int SeqListEmpty(SeqList* ps);

// 13.获取顺序表中的第一个元素
DataType SeqListFront(SeqList* ps);

// 14.获取顺序表中最后一个元素
DataType SeqListBack(SeqList* ps);

// 15.获取顺序表中任意位置的元素
DataType SeqListGet(SeqList* ps, int pos);
///////////////////////////////////////////////////////////

// 测试顺序表
void TestSeqList();