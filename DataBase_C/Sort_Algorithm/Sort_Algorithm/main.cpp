#include <iostream>
#include "mysort.h"

void test()
{
	int a[] = {5,2,3,1 };
	int sz = sizeof(a) / sizeof(a[0]);
	
	// InsertSort(a, sz);       //直接插入
	// ShellSort(a, sz);        //希尔（增量缩小插入）
	// SelectSort(a, sz);       //直接选择
	// SelectSortPlus(a, sz);   //直接选择优化
	// HeapSort(a, sz);         //堆排序 
	// BubbleSort(a, sz);       //冒泡 
	// QuickSort(a, 0, sz);     //快速排序-递归
	// QuickSortNonR(a, sz);    //快速排序-非递归
	// MergeSort(a, sz);        //归并-递归
	// MergeSortNonR(a, sz);    //归并-非递归
	CountSort(a, sz);           //计数排序

	for (int i = 0; i < sz; ++i)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

int main()
{
	test();
	return 0;
}