#include <iostream>
#include <stack>

//-----------------插入排序-----------------//

// 1、直接插入排序
//		时间复杂度O(N^2)
//		空间复杂度O(1)
//		稳定排序
void InsertSort(int* a, int n)
{
	// 默认第一个元素已有序
	for (int i = 1; i < n; ++i)
	{
		int key = a[i];  //标记无序队列第一个元素
		int end = i - 1;  //有序队列最后一个元素
		while (end >= 0 && a[end] > key)
		{
			a[end + 1] = a[end];
			end--;
		}
		a[end + 1] = key;
	}
}

// 2、希尔排序( 缩小增量排序 )
//		时间复杂度 N1.25~1.6*N1.25
//		空间复杂度 O(1)
//		不稳定

// 步骤：
// a. 定义gap，将数组下标%gap相等的数分为一组
// b. 对每一组采用插入排序
// c. gap更新：gap/3 + 1，直至减小为1
void ShellSort(int* a, int n)
{
	int gap = n;
	
	// 1.挨个遍历步长，缩短步长，直到步长为零
	while (gap > 1)
	{
		// 更新gap
		gap = gap / 3 + 1;
		// 2.遍历所有的元素（每组元素的下标相差gap）
		for (int i = gap; i < n; ++i)
		{
			int key = a[i];
			int end = i - gap;
			// 3.遍历本组中所有有序的元素
			while (end >= 0 && a[end]>key)
			{
				a[end + gap] = a[end];
				end -= gap;
			}
			a[end + gap] = key;
		}
	}
}

//-----------------选择排序-----------------//

// 3、直接选择排序（优化）
//		时间复杂度 O(N^2)
//		空间复杂度 O(1)
//		不稳定

// 步骤：
// a. 遍历数组，找到数组中最大和最小的两个元素（普通版本的选择排序一次只会从中选出一个元素（最大或最小））
// b. 将最大元素与数组末尾元素交换，将最小元素与数组开头元素交换
// c. 循环，直到对数组完成一次遍历后，排序结束
static void Swap(int& left, int& right)
{
	int temp = left;
	left = right;
	right = temp;
}

// 普通版本
void SelectSort(int* a, int n)
{
	// 每次循环，规模减小
	for (int i = 0; i < n-1; ++i)
	{
		int MaxPos = 0;
		// 循环找最大
		for (int j = 0; j < n - i; ++j)
		{
			if (a[MaxPos] < a[j])
			{
				MaxPos = j;
			}
		}
		// 将最大值置于最右
		if (MaxPos != n - 1 - i)
		{
			Swap(a[MaxPos], a[n - 1 - i]);
		}
	}
}

// 优化版本
void SelectSortPlus(int* a, int n)
{
	// 每次循环，规模减小
	for (int left = 0, right = n-1; left < right; left++, right--)
	{
		int maxPos = right, minPos = left;
		int index = left + 1;
		// 循环找最大、最小
		for (int i = left; i <= right; ++i)
		{
			if (a[maxPos] < a[i])
			{
				maxPos = i;
			}
			if (a[minPos] > a[i])
			{
				minPos = i;
			}
		}
		// 更新位置
		if (maxPos != right)
		{
			Swap(a[maxPos], a[right]);
		}
		// 这里要注意防止right开始为最小值
		if (minPos == right)
		{
			minPos = maxPos;  //需将minPos更新为交换后的maxPos位置
		}
		if (minPos != left)
		{
			Swap(a[minPos], a[left]);
		}
	}
}

// 4、堆排序
//		时间复杂度 O(NlogN)
//		空间复杂度 O(1)
//		不稳定

// 步骤：
// 升序建大堆（堆是一个特殊的完全二叉树）
// 利用堆删除思想进行排序
// a. 将堆顶元素与最后一个元素交换（得到最大值）
// b. 将元素规模-1
// c. 重新将堆向下调整算法，使其成为新的堆
static void AdjustDwon(int* a, int n, int parent)
{
	// 标记child指向左孩子
	int child = parent * 2 + 1;
	while (child < n)
	{
		// 判断是否有右孩子，并让child指向较大的结点
		if (child + 1 < n && a[child + 1] > a[child])
		{
			child += 1;
		}
		// 更新父节点和孩子节点
		if (a[child] > a[parent])
		{
			Swap(a[child], a[parent]);
			parent = child;
			child = 2 * parent + 1;
		}
		else
		{
			// 已符合堆结构，直接返回
			return;
		}
	}
}
void HeapSort(int* a, int n)
{
	// 建大堆
	// 初始化建堆只需要对二叉树的非叶子节点调用adjusthead()函数，由下至上，由右至左选取非叶子节点来调用adjusthead()函数
	for (int parent = (n - 2) / 2; parent >= 0; --parent)
	{
		AdjustDwon(a, n, parent);
	}
	// 利用堆删除思想排序
	int end = n;
	while (end--)
	{
		Swap(a[0], a[end]);
		// 重新构建堆
		AdjustDwon(a, end, 0);
	}
}

//-----------------交换排序-----------------//

// 5、冒泡排序
//		时间复杂度 O(N2)
//		空间复杂度 O(1)
//		稳定

// 步骤：
// 两两比较，一趟遍历找到一个元素的最终位置（最大或最小）
void BubbleSort(int* a, int n)
{
	for (int i = 0; i < n - 1; ++i)
	{
		int flag_change = 0;
		for (int j = 0; j < n - 1 - i; ++j)
		{
			if (a[j] > a[j + 1])
			{
				flag_change = 1;
				Swap(a[j], a[j + 1]);
			}
		}
		if (flag_change == 0)  //若次轮没有元素交换，说明后续元素均有序
		{
			return;
		}
	}
}

// 6、快速排序——搜索二叉树
//		时间复杂度 O(NlogN)
//		空间复杂度 O(logN)
//		不稳定

// 递归实现
// 步骤：
// a. 任取待排序元素序列中的某元素作为基准值，将该组数据小于基准值的划分子在基准值的左侧，大于基准值的划分在基准值的右侧
// b. 递归：
//    使用快排对基准值的左侧进行排序
//    使用快排对基准值的右侧进行排序

// 基准值的选择：采用三数取中法返回基准值下标（最大程度让基准值分出的两半数据相等）
// 即：取数组左右端点以及中间元素三者中值居中的作为基准值
// 注：一般情况下找到的基准值要是不在数组末尾，将基准值与末尾元素交换（确保基准值始终在数组的末尾）
static int GetMidPos(int* a, int left, int right)
{
	int mid = left + ((right - left) >> 1);
	if (a[left] < a[right - 1])
	{
		if (a[left] >= a[mid])
		{
			return left;
		}
		else if (a[right - 1] <= a[mid])
		{
			return right - 1;
		}
		else
		{
			return mid;
		}
	}
	else
	{
		if (a[left] <= a[mid])
		{
			return left;
		}
		else if (a[right - 1] >= a[mid])
		{
			return right - 1;
		}
		else
		{
			return mid;
		}
	}
}


// 三种数据分割接口：（该接口时间复杂度 O(N)）
// 1. 快速排序hoare版本
//	  a. 让begin从前向后遍历，找到大于基准值的元素停下来
//    b. 让end从后向前遍历，找到小于基准值的元素停下来
//    c. 将begin和end所指向的元素交换
//    d. 循环1、2、3步，直到begin与end相遇，划分结束
static int PartSort1(int* a, int left, int right)
{
	int begin = left;
	int end = right - 1;
	int mid = GetMidPos(a, left, right);
	// 让基准值的位置置于最后（最前也行，不能在中间）
	if (mid != end)
	{
		Swap(a[mid], a[end]);
	}
	int key = a[end];
	while (begin < end)
	{
		while (begin < end && a[begin] <= key)
		{
			begin++;
		}
		while (begin < end && a[end] >= key)
		{
			end--;
		}
		if (begin < end)
		{
			Swap(a[begin], a[end]);
		}
	}
	// begin和end相遇，循环出来，此时begin的值一定是>=key的
	// 将key（在末尾）置于begin所在的位置
	if (begin != right - 1)
	{
		Swap(a[begin], a[right - 1]);
	}
	return begin;
}

// 2. 快速排序挖坑法（主要思想与hoare版本相似，时间复杂度 O(N)）
//    a. 将基准值保存到key中，此时数组基准值所在位置相当于可以占的“坑位”，用end标记
//    b. begin从前向后遍历，找到大于基准值key的元素后停止，然后将该元素赋值给空着的坑位end，此时，begin所指的位置称为新的坑位
//	  c. end指针从后向前移动，遇到小于基准值key的元素后，将其赋值给新的坑位begin
//    d. 循环执行上述操作，直到begin与end相遇时，将key值赋值到begin所在位置，一次划分结束
static int PartSort2(int* a, int left, int right)
{
	int begin = left;
	int end = right - 1;
	int mid = GetMidPos(a, left, right);
	// 让基准值的位置置于最后（最前也行，不能在中间）
	if (mid != end)
	{
		Swap(a[mid], a[end]);
	}
	int key = a[end];
	while (begin < end)
	{
		while (begin < end && a[begin] <= key)
		{
			begin++;
		}
		if (begin < end)
		{
			a[end] = a[begin];
		}
		while (begin < end && a[end] >= key)
		{
			end--;
		}
		if (begin < end)
		{
			a[begin] = a[end];
		}
	}
	// 循环退出，begin的位置是个坑位，放入基准值
	a[begin] = key;
	return begin;
}

// 3. 快速排序前后指针法
//    a. 初始两个指针prev和cur，cur指向第一个元素，prev在cur前一个
//    b. cur指 >= key的元素，prev指的是 < key的最后一个元素
//    c. 当a[cur] < key 且 ++prev的位置不是cur，说明此时[prev, cur-1]的元素均 >= key
//    d. 因此需要交换 cur 和 prev的元素
//    e. 循环执行直至cur 到达 最后一个元素（right-1）
static int PartSort3(int* a, int left, int right)
{
	int cur = left;
	int prev = cur - 1;
	int end = right - 1;
	int mid = GetMidPos(a, left, right);
	// 让基准值的位置置于最后（最前也行，不能在中间）
	if (mid != end)
	{
		Swap(a[mid], a[end]);
	}
	int key = a[end];
	while (cur <= end)
	{
		if (a[cur] < key && ++prev != cur)
		{
			Swap(a[cur], a[prev]);
		}
		cur++;
	}
	// 退出循环，prev是小于key的最后一位，因此将prev+1的位置置于key
	if (++prev != end)
	{
		Swap(a[prev], a[end]);
	}
	return prev;
}

// 快速排序的递归实现：（递归 logN 次）
// 按照升序对array数组中[left, right)区间中的元素进行排序
void QuickSort(int* a, int left, int right)
{
	if (right - left <= 1)
	{
		return;
	}
	//根据Partion对数组进行划分--->让小于div的元素位于div的左侧，大于等于div的元素位于div的右侧
	//主要划分方式有 hoare版本  挖坑法  前后指针法
	int div = PartSort1(a, left, right);
	QuickSort(a, left, div);
	QuickSort(a, div + 1, right);
}

// 非递归实现
// a. 与递归的思路基本一致，只不过是采用栈这一数据结构来存储待处理元素的下标范围
// b. 先存储右区间，再存储左区间。每个区间先存储右边界，再存储左边界
void QuickSortNonR(int* a, int n)
{
	int begin = 0;
	int end = n;
	
	std::stack<int> s;
	s.push(end);
	s.push(begin);

	while (!s.empty())
	{
		begin = s.top();
		s.pop();
		end = s.top();
		s.pop();
		if (begin < end)
		{
			// div将数组分为左右两部分
			int div = PartSort1(a, begin, end);
			// 处理顺序先左后右
			// 因此新边界入栈顺序为先右后左
			s.push(end);
			s.push(div + 1);
			s.push(div);
			s.push(begin);
		}
	}
}

//-----------------外部排序-----------------//

// 7、归并排序
// 是一种外部排序
// 类似平衡二叉树
// 时间复杂度：O(N*logN) 归并的总耗时 树的高度*N = logN*N
// 空间复杂度：O(N) + O(logN)  化简=>O(N)
// 稳定性：稳定

// 归并排序递归实现
// 步骤：
// a. 均分 将待排序数组不断均分，分为左右两半部分，直到每一部分都是有序（仅有一个元素）
// b. 归并。情形：合并两个有序数组
// c. 将存储在临时数组中的元素copy至原数组


// 合并接口
void MergeLink(int* a, int left, int mid, int right, int* temp)
{
	// 双指针遍历
	int begin1 = left, end1 = mid;
	int begin2 = mid, end2 = right;
	int index = left;
	while (begin1 < end1 && begin2 < end2)
	{
		if (a[begin1] <= a[begin2])
		{
			temp[index++] = a[begin1++];
		}
		else
		{
			temp[index++] = a[begin2++];
		}
	}
	while (begin1 < end1)
	{
		temp[index++] = a[begin1++];
	}
	while (begin2 < end2)
	{
		temp[index++] = a[begin2++];
	}
}

// 递归接口
void MergeSort(int* a, int left, int right, int* temp)
{
	int mid = left + ((right - left) >> 1);
	if (right - left > 1)  //注意这里right-left == 1不能进入递归，因为是左闭右开区间
	{
		// 递归分解为左右两部分
		MergeSort(a, left, mid, temp);
		MergeSort(a, mid, right, temp);

		// 合并
		MergeLink(a, left, mid, right, temp);
		// 合并的数据拷贝至原数组
		memcpy(a + left, temp + left, (right - left)*sizeof(a[0]));
	}
}

void MergeSort(int* a, int n)
{
	int* temp = new int[n];
	MergeSort(a, 0, n, temp);  //调用递归接口
	delete(temp);
}

// 归并排序非递归实现
// 步骤：
// a. 将待排序数组的每一个元素看做是一个独立的个体，那么他们每个都是有序的
// b. 对每一个部分直接归并，直接归并分三步：
//		采用gap来标记每次每组参与归并的元素个数
//		gap从1开始，每归并完成一次，gap*2
//		直到gap的值大于数组元素个数size时，说明排序完成
//    注：随着gap的不断增大，归并函数中边界变量可能会越界，需要做合法性判断
// c. 每归并一次，将临时数组中的元素拷贝至原数组
void MergeSortNonR(int* a, int n)
{
	int* temp = new int[n];
	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < n; i += 2 * gap)
		{
			int left = i;
			int mid = left + gap;
			int right = mid + gap;
			// 边界判断
			if (mid > n)
			{
				mid = n;
			}
			if (right > n)
			{
				right = n;
			}
			// 合并
			MergeLink(a, left, mid, right, temp);
		}
		// 合并的数据拷贝至原数组
		memcpy(a, temp, n*sizeof(a[0]));
		gap *= 2;
	}
	delete(temp);
}

//-----------------非比较排序-----------------//

// 8、计数排序（哈希）
//		时间复杂度 O(N)
//		空间复杂度 O(M) M为数据所给范围
//		稳定
//      适用于：数据集中在一个小数据段中

// 步骤：
// 通过辅助数组，保存每个元素出现的次数
void CountSort(int* a, int n)
{
	// 找最大值、最小值确定辅助数组范围
	int maxNum = a[0], minNum = a[0];
	for (int i = 0; i < n; ++i)
	{
		maxNum = maxNum >= a[i] ? maxNum : a[i];
		minNum = minNum <= a[i] ? minNum : a[i];
	}
    // 构造辅助数组
	int len = maxNum - minNum + 1;
	int* temp = new int[len];
	// 初始化数组为0
	memset(temp, 0, sizeof(int)*len);
	
	// 遍历待排序数组
	for (int i = 0; i < n; i++)
	{
		temp[a[i] - minNum]++;
	}
	// 遍历辅助数组，排序原数组元素
	int index = 0;
	for (int i = 0; i < len; ++i)
	{
		while (temp[i]--)
		{
			a[index++] = i + minNum;
		}
	}
	// 释放辅助空间
	delete temp;
}