#include "Stack.h"


////////////////////////////////////////////////////////////////////
//检测空间是否已满
static void CheckCapacity(Stack* ps)
{
	assert(ps);
	int newCapacity = 2 * ps->capacity;
	if (ps->size == ps->capacity)
	{
		//1.申请新空间
		STDataType* newArray = (STDataType*)malloc(sizeof(STDataType) * newCapacity);
		if (NULL == newArray)
		{
			assert(0);
			return;
		}
		//2.拷贝原空间至新空间
		memcpy(newArray, ps->array, ps->size * sizeof(STDataType));
		//3.释放原空间
		free(ps->array);

		ps->array = newArray;
		ps->capacity = newCapacity;
	}
}

// 检测栈是否为空
int StackEmpty(Stack* ps)
{
	assert(ps);
	return 0 == ps->size;
}


//初始化链表
void StackInit(Stack* ps)
{
	assert(ps);
	ps->array = (STDataType*)malloc(sizeof(STDataType) * 3); //初始化默认开辟三个单位
	if (NULL == ps->array)
	{
		assert(0);
		return;
	}

	ps->capacity = 3;
	ps->size = 0;
}

//销毁链表
void StackDestroy(Stack* ps)
{
	assert(ps);
	if (ps->array)
	{
		free(ps->array);
		ps->array = NULL;
		ps->capacity = 0;
		ps->size = 0;
	}
}

// 入栈 （尾插）
void StackPush(Stack* ps, STDataType data)
{
	assert(ps);
	//1.判断栈空间
	CheckCapacity(ps);
	//2.直接插入
	ps->array[ps->size] = data;
	ps->size++;
}

// 出栈 （尾删）
void StackPop(Stack* ps)
{
	assert(ps);
	//1.检测栈是否为空
	if (StackEmpty(ps))
	{
		printf("删除错误！栈为空\n");
		return;
	}
	//2.直接减值
	ps->size--;
}

// 获取栈顶元素
STDataType StackTop(Stack* ps)
{
	assert(ps);
	//1.检测栈是否为空
	if (StackEmpty(ps))
	{
		printf("获取错误！栈为空\n");
		return -1;
	}
	//2.返回数组最后一位元素
	return ps->array[ps->size - 1];
}

// 获取栈中有效元素个数
int StackSize(Stack* ps)
{
	assert(ps);
	return ps->size;
}


/////////////////////////////////////////////////////////////////////
static void test1()
{
	Stack s;
	StackInit(&s);

	StackPush(&s, 1);
	StackPush(&s, 2);
	StackPush(&s, 3);
	StackPush(&s, 4);

	printf("%d\n", StackTop(&s));
	printf("%d\n", StackSize(&s));

	StackPop(&s);
	printf("%d\n", StackTop(&s));
	printf("%d\n", StackSize(&s));

	StackDestroy(&s);
}

//测试函数
void TestStatk()
{
	test1();
}