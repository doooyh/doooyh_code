#pragma once

#include<stdio.h>
#include<Windows.h>
#include<assert.h>
#include<malloc.h>
#pragma warning(disable:4996)

//栈是一种前出后进的线性表 可以通过顺序表和链表实现（一般采用顺序表）

typedef int STDataType;
 
//采用动态开辟顺序表结构
typedef struct Stack
{
	STDataType* array;
	int capacity;  //空间总大小
	int size;  //储存实际大小
}Stack;

////////////////////////////////////////////////////////////////////
//初始化链表
void StackInit(Stack* ps);

//销毁链表
void StackDestroy(Stack* ps);

// 入栈
void StackPush(Stack* ps, STDataType data);

// 出栈
void StackPop(Stack* ps);

// 获取栈顶元素
STDataType StackTop(Stack* ps);

// 获取栈中有效元素个数
int StackSize(Stack* ps);

// 检测栈是否为空
int StackEmpty(Stack* ps);

/////////////////////////////////////////////////////////////////////


//测试函数
void TestStatk();
