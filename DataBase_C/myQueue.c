#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include <Windows.h>


//力扣 232. 用栈实现队列
//https://leetcode-cn.com/problems/implement-queue-using-stacks/

//栈结构只从尾部出入 因此采用顺序表结构
//采用动态开辟顺序表结构
typedef struct Stack
{
	int* array;
	int capacity;  //空间总大小
	int size;  //储存实际大小
}Stack;

// 初始化栈
void StackInit(Stack* ps)
{
	assert(ps);
	ps->array = (int*)malloc(sizeof(int) * 3); //初始化默认开辟三个单位
	if (NULL == ps->array)
	{
		assert(0);
		return;
	}

	ps->capacity = 3;
	ps->size = 0;
}

// 检测空间是否已满
static void CheckCapacity(Stack* ps)
{
	assert(ps);
	int newCapacity = 2 * ps->capacity;
	if (ps->size == ps->capacity)
	{
		//1.申请新空间
		int* newArray = (int*)malloc(sizeof(int) * newCapacity);
		if (NULL == newArray)
		{
			assert(0);
			return;
		}
		//2.拷贝原空间至新空间
		memcpy(newArray, ps->array, ps->size * sizeof(int));
		//3.释放原空间
		free(ps->array);

		ps->array = newArray;
		ps->capacity = newCapacity;
	}
}

// 入栈 （尾插）
void StackPush(Stack* ps, int data)
{
	assert(ps);
	//1.判断栈空间
	CheckCapacity(ps);
	//2.直接插入
	ps->array[ps->size] = data;
	ps->size++;
}

// 检测栈是否为空
int StackEmpty(Stack* ps)
{
	assert(ps);
	return 0 == ps->size;
}

// 出栈 （尾删）
int StackPop(Stack* ps)
{
	assert(ps);
	//1.检测栈是否为空
	if (StackEmpty(ps))
	{
		printf("删除错误！栈为空\n");
		return -1;
	}
	//2.保存删除的值
	int delData = ps->array[ps->size - 1];
	//3.直接减值
	ps->size--;

	return delData;
}

// 获取栈顶元素
int StackTop(Stack* ps)
{
	assert(ps);
	//1.检测栈是否为空
	if (StackEmpty(ps))
	{
		printf("获取错误！栈为空\n");
		return -1;
	}
	//2.返回数组最后一位元素
	return ps->array[ps->size - 1];
}

//销毁链表
void StackDestroy(Stack* ps)
{
	assert(ps);
	if (ps->array)
	{
		free(ps->array);
		ps->array = NULL;
		ps->capacity = 0;
		ps->size = 0;
	}
}

//-------------------------------------------------------------------------------

//定义队列结构--两个栈构成
typedef struct
{
	Stack S1;  //令S1为入队栈
	Stack S2;  //令S2为出队栈
} MyQueue;

//初始化队列
MyQueue* myQueueCreate()
{
	//先在堆上开辟队列空间
	MyQueue* obj = (MyQueue*)malloc(sizeof(MyQueue));
	if (NULL == obj)
	{
		assert(0);
		return NULL;
	}
	//初始化队列里的栈空间
	StackInit(&(obj->S1));
	StackInit(&(obj->S2));

	return obj;
}

//入队
//直接将数据存入栈S1中
void myQueuePush(MyQueue* obj, int x)
{
	assert(obj);
	StackPush((&(obj->S1)), x);
}

//出队
//将栈S2的栈顶出栈
//若S2没有数据，则先将栈S1的所有数据转入栈S2中
int myQueuePop(MyQueue* obj) 
{
	if (StackEmpty(&(obj->S2)))  //栈S2为空
	{
		while (obj->S1.size)
		{
			StackPush(&(obj->S2), StackPop(&(obj->S1)));
		}
	}
	return StackPop(&(obj->S2));
}

//返回队列开头的元素
//返回栈S2的栈顶元素
//若S2没有数据，则先将栈S1的所有数据转入栈S2中
int myQueuePeek(MyQueue* obj) 
{
	assert(obj);
	if (StackEmpty(&(obj->S2)))  //栈S2为空
	{
		while (obj->S1.size)
		{
			StackPush(&(obj->S2), StackPop(&(obj->S1)));
		}
	}
	return StackTop(&(obj->S2));
}

//判断队列是否为空
//直接判断两个栈是否为空即可
bool myQueueEmpty(MyQueue* obj) 
{
	assert(obj);
	if (StackEmpty(&(obj->S1)) && StackEmpty(&(obj->S2)))
	{
		return true;
	}
	return false;
}

void myQueueFree(MyQueue* obj) 
{
	assert(obj);
	StackDestroy(&(obj->S1));
	StackDestroy(&(obj->S2));
	free(obj);
}


void Test1()
{
	MyQueue* mQ = myQueueCreate();

	myQueuePush(mQ, 1);
	myQueuePush(mQ, 2);
	printf("队首：%d\n", myQueuePeek(mQ));

	printf("出队：%d\n", myQueuePop(mQ));
	printf("队首：%d\n", myQueuePeek(mQ));
	if (myQueueEmpty(mQ))
	{
		printf("true\n");
	}
	else
	{
		printf("false\n");
	}

	printf("出队：%d\n", myQueuePop(mQ));
	printf("队首：%d\n", myQueuePeek(mQ));
	if (myQueueEmpty(mQ))
	{
		printf("true\n");
	}
	else
	{
		printf("false\n");
	}

	myQueueFree(mQ);
}

int main()
{
	Test1();

	system("pause");
	return 0;
}
