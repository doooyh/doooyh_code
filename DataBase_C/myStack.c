//力扣225. 用队列实现栈
//https://leetcode-cn.com/problems/implement-stack-using-queues/


//两队列实现栈结构

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <Windows.h>

//用队列模拟栈结构，那么结点应该用队列的结点结构
//队列是前出后进 也就是头删尾插
//头删更适合链表结构

// 定义单链表结点
typedef struct QNode
{
	int data;
	struct QNode* next;
}QNode;

// 定义队列结构体
typedef struct Queue
{
	QNode* front;  //指向队头结点 队头：出队
	QNode* rear;   //指向队尾结点 队尾：入队
	int size;      //队列元素个数
}Queue;

// 初始化队列
void QueueInit(Queue* q)
{
	assert(q);
	q->front = NULL;
	q->rear = NULL;
	q->size = 0;
}

// 检测队列是否为空
int QueueEmpty(Queue* q)
{
	assert(q);
	return 0 == q->size;
}

// 创建队列结点
QNode* buyQNode(int data)
{
	QNode* newQNode = (QNode*)malloc(sizeof(QNode));
	if (NULL == newQNode)
	{
		assert(0);
		return NULL;
	}
	newQNode->data = data;
	newQNode->next = NULL;

	return newQNode;
}

// 入队列 尾插
void QueuePush(Queue* q, int data)
{
	assert(q);
	QNode* newQNode = buyQNode(data);
	//1.队列为空
	if (QueueEmpty(q))
	{
		q->front = newQNode;
	}
	else
	{
		//2.队列已有元素
		q->rear->next = newQNode;
	}
	//3.调整队列其余元素
	q->rear = newQNode;
	q->size++;

	return;
}

// 出队列 头删
int QueuePop(Queue* q)
{
	assert(q);
	//1.队列为空
	if (QueueEmpty(q))
	{
		assert(0);
		return -1;
	}
	else
	{
		//2.队列已有元素
		QNode* delNode = q->front;
		int delData = delNode->data;
		q->front = q->front->next;
		q->size--;
		free(delNode);
		if (NULL == q->front)
		{
			q->rear = NULL;
		}
		return delData;
	}
}

// 获取队尾元素
int QueueBack(Queue* q)
{
	//队列为空情况设为违法
	assert(!QueueEmpty(q));
	return q->rear->data;
}

// 销毁队列
void QueueDestroy(Queue* q)
{
	assert(q);
	QNode* cur = q->front;
	while (cur)
	{
		q->front = cur->next;
		free(cur);
		cur = q->front;
	}
	q->front = NULL;
	q->rear = NULL;
	q->size = 0;
}

//------------------------------------------------------------------------------

//定义栈结构--由两个队列构成
typedef struct
{
	Queue Q1;
	Queue Q2;
} MyStack;


//创建栈
MyStack* myStackCreate()
{
	MyStack* obj = (MyStack*)malloc(sizeof(MyStack));  //在堆上开辟栈结构
	if (NULL == obj)
	{
		assert(0);
		return NULL;
	}
	QueueInit(&(obj->Q1));  //初始化队列1
	QueueInit(&(obj->Q2));  //初始化队列2
	return obj;
}

//入栈
//哪个队列不为空，存进那个队
void myStackPush(MyStack* obj, int x)
{
	if (QueueEmpty(&(obj->Q1)))
	{
		QueuePush(&(obj->Q2),x);
	}
	else
	{
		QueuePush(&(obj->Q1), x);
	}
}

//出栈
//将不为空队列的前n-1个结点转到另一个队列
//在将该队列最后一个结点出队即可
int myStackPop(MyStack* obj)
{
	if (QueueEmpty(&(obj->Q1)))
	{
		while (obj->Q2.size > 1)
		{
			QueuePush(&(obj->Q1), QueuePop(&(obj->Q2)));
		}
		return QueuePop(&(obj->Q2));
	}
	while (obj->Q1.size > 1)
	{
		QueuePush(&(obj->Q2), QueuePop(&(obj->Q1)));
	}
	return QueuePop(&(obj->Q1));
}

//返回栈顶元素
//将不为空的队列的队尾元素返回即可
int myStackTop(MyStack* obj) 
{
	if (QueueEmpty(&(obj->Q1)))
	{
		return QueueBack(&(obj->Q2));
	}
	else
	{
		return QueueBack(&(obj->Q1));
	}
}

//判断是否空栈
//判断两个队列是否为空
bool myStackEmpty(MyStack* obj) 
{
	if (QueueEmpty(&(obj->Q1)) && QueueEmpty(&(obj->Q2)))
	{
		return true;
	}
	return false;
}

//释放栈
//先销毁栈的两个队列，再销毁栈结构
void myStackFree(MyStack* obj) 
{
	assert(obj);
	QueueDestroy(&(obj->Q1));
	QueueDestroy(&(obj->Q2));

	free(obj);
}

void test1()
{
	MyStack* mystack = myStackCreate();
	myStackPush(mystack, 1);
	myStackPush(mystack, 2);

	printf("栈顶：%d\n", myStackTop(mystack));
	printf("出栈：%d\n", myStackPop(mystack));

	if (myStackEmpty(mystack))
	{
		printf("true\n");
	}
	else
	{
		printf("false\n");
	}
	
	printf("出栈：%d\n", myStackPop(mystack));

	if (myStackEmpty(mystack))
	{
		printf("true\n");
	}
	else
	{
		printf("false\n");
	}
}

int main()
{
	test1();

	system("pause");
	return 0;
}


/**
 * Your MyStack struct will be instantiated and called as such:
 * MyStack* obj = myStackCreate();
 * myStackPush(obj, x);

 * int param_2 = myStackPop(obj);

 * int param_3 = myStackTop(obj);

 * bool param_4 = myStackEmpty(obj);

 * myStackFree(obj);
*/