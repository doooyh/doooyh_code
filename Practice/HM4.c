//HM4 打印菱形

#define  _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<Windows.h>

int main()
{
	printf("请输入需要打印菱形行数（奇数）：");
	int n;
	scanf("%d", &n);

	for (int i = 0; i <= n / 2; i++) //打印上层
	{
		for (int j = 0; j < n / 2 - i; j++)
		{
			printf(" ");
		}
		for (int j = 0; j < 2 * i + 1; j++)
		{
			printf("*");
		}
		printf("\n");
	}
	for (int i = 0; i < n / 2; i++) //打印下层
	{
		for (int j = 0; j < i + 1; j++)
		{
			printf(" ");
		}
		for (int j = 0; j < n - 2 * (i + 1); j++)
		{
			printf("*");
		}
		printf("\n");
	}

	system("pause");
	return 0;
}