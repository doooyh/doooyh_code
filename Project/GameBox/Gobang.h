#pragma once

#include<stdio.h>
#include<Windows.h>
#pragma warning(disable:4996)

#define ROW_G 10    //五子棋棋盘行数
#define COL_G 10    //五子棋棋盘列数
#define USER1 1    //用户1标志
#define USER2 2    //用户2标志
#define NEXT_G 3   //继续
#define DRAW_G 4   //平局

#define UP 10
#define RIGHT_UP 11
#define RIGHT  12
#define RIGHT_DOWN 13
#define DOWN 14
#define LEFT_DOWN 15
#define LEFT 16
#define LEFT_UP 17

extern void Gobang();