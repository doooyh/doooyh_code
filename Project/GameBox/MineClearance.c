#include"MineClearance.h"

static void SetMine(char board[][COL])
{
	int count = NUM;
	while (count)
	{
		int x = rand() % (ROW - 2) + 1; //下标范围[1 COL-2]
		int y = rand() % (COL - 2) + 1;
		if (board[x][y] == '0')
		{
			board[x][y] = '1';
			count--;
		}
	}
}

static void ShowLine(int col)  //使Showboard函数能够根据宏定义自适应大小
{
	printf("   +");
	for (int i = 0; i < (col - 2); i++){
		printf("----");
	}
	printf("+\n");
}

static void ShowBoard(char board[][COL])
{
	printf("     ");
	for (int i = 1; i <= (COL - 2); i++)
	{
		printf("%d   ", i);
	}
	printf("\n");
	ShowLine(COL);
	for (int i = 1; i <= (ROW - 2); i++)
	{
		printf("%-3d|", i);
		for (int j = 1; j <= (COL - 2); j++)
		{
			printf(" %c |", board[i][j]);
		}
		printf("\n");
		ShowLine(COL);
	}
}

static char CountMines(char board[][COL], int x, int y)  //返回周边炸弹个数
{
	return board[x - 1][y - 1] + board[x - 1][y] + board[x - 1][y + 1] + \
		board[x][y + 1] + board[x + 1][y + 1] + board[x + 1][y] + \
		board[x + 1][y - 1] + board[x][y - 1] - 7 * '0';
}

void MineClearance()
{
	srand((unsigned long)time(NULL));  //随机数种子
	//初始化两个相同表格，一个对玩家显示，一个随机生成答案
	char show_board[ROW][COL];  
	char mine_board[ROW][COL];
	//借用memset函数对二进制数组初始化
	memset(show_board, FUN, sizeof(show_board));  //玩家显示表格显示：？
	memset(mine_board, '0', sizeof(mine_board));  //答案表格初始化为0

	SetMine(mine_board);  //随机在答案表格生成炸弹坐标

	int count = (ROW - 2)*(COL - 2) - NUM;
	while (count)
	{
		system("cls");
		ShowBoard(show_board);
		printf("Please Enter Your Postion<x,y>: ");
		int x = 0;
		int y = 0;
		scanf("%d %d", &x, &y);
		if (x < 1 || x > COL || y < 1 || y > ROW)
		{
			printf("Postion Error!\n");
			continue;
		}
		if (show_board[x][y] != FUN)
		{
			printf("Postion Is not *\n");
			continue;
		}
		if (mine_board[x][y] == '1')
		{
			printf("game over!\n");
			ShowBoard(mine_board);
			break;
		}
		show_board[x][y] = CountMines(mine_board, x, y);  //提示功能函数
		count--;
	}
}