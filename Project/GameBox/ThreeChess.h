#pragma once

#include<stdio.h>
#include<Windows.h>
#pragma warning(disable:4996)

#include <time.h>
#include <stdlib.h>
#define ROW 3      //三子棋行数
#define COL 3      //三子棋列数
#define NEXT 'N'   //继续
#define DRAW 'W'   //平局
#define BLACK 'X'  //用户下黑棋 X
#define WHITE 'O'  //电脑下白棋 O
#define INIT ' '   //初始化空格


extern void ThreeChess();