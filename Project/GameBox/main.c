#include "ThreeChess.h"
#include "Gobang.h"
#include"MineClearance.h"

void Menu()
{
	printf("+-----------------------+\n");
	printf("|       1.三子棋        |\n");
	printf("|       2.五子棋        |\n");
	printf("|       3.扫雷游戏      |\n");
	printf("|       0.Exit !        |\n");
	printf("+-----------------------+\n");
}

int main()
{
	int select = 0;
	int quit = 0;
	while (!quit)
	{
		printf("请输入游戏选项：\n");
		Menu();
		scanf("%d", &select);
		printf("\n");
		switch (select)
		{
		case 1:
			ThreeChess();
			break;
		case 2:
			Gobang();
			break;
		case 3:
			MineClearance();
			break;
		case 0:
			quit = 1;
			break;
		default:
			printf("输入选项错误！请重新选择\n");
			break;
		}
	}
	printf("再见！");

	system("pause");
	return 0;
}