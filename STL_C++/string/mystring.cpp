#include "mystring.h"
#include <assert.h>
#include <iostream>
#pragma warning(disable:4996)

// 注意：
// 编译器禁止声明和定义时同时定义缺省参数值，因此这里函数定义出不带缺省参数

// 1.构造和析构
// 1.1带参构造函数
mySpace::string::string(const char* str)
{
	// 若传入空指针，无法直接调用strlen
	if (nullptr == str)
	{
		assert(0);
	}
	_size = strlen(str);
	_capacity = _size;
	_str = new char[strlen(str) + 1];
	strcpy(_str, str);
}
// 1.2拷贝构造函数
mySpace::string::string(const mySpace::string& s)
: _str(nullptr)
, _size(0)
, _capacity(0)
{
	mySpace::string temp(s._str);
	swap(temp);
}
// 1.3赋值重载函数
mySpace::string& mySpace::string::operator=(const mySpace::string& s)
{
	if (this != &s)
	{
		mySpace::string temp(s._str);
		swap(temp);
	}
	return *this;
}
// 1.4析构函数
mySpace::string::~string()
{
	if (_str)
	{
		delete[] _str;
		_str = nullptr;
	}
}

// 2.迭代器——仅实现正向迭代器
// iterator是返回值类型
// begin和end才是函数名
mySpace::string::iterator mySpace::string::begin()const
{
	return _str;
}
mySpace::string::iterator mySpace::string::end()const
{
	return _str + _size;
}

// 3.容量
// 3.1返回对象有效元素个数
size_t mySpace::string::size()const
{
	return _size;
}
// 3.2返回对象容量大小
size_t mySpace::string::capacity()const
{
	return _capacity;
}
// 3.3判断对象是否为空串
bool mySpace::string::empty()const
{
	return _size == 0;
}
// 3.4修改对象有效元素个数
void mySpace::string::resize(size_t n, char c)
{
	if (n > _size)
	{
		if (n > _capacity)
		{
			// 需扩容
			reserve(n);
		}
		memset(_str + _size, c, n - _size);
	}
	_size = n;
	_str[_size] = '\0';  //注意需将最后一位主动置为'\0'

}
// 3.5修改对象容量大小
void mySpace::string::reserve(size_t n)
{
	if (n > _capacity)
	{
		char* temp = new char[n + 1];
		strcpy(temp, _str);
		delete[] _str;
		_str = temp;
		_capacity = n;
	}
}

// 4.元素访问
// operator[]就是函数名
// 4.1普通对象的元素访问
char& mySpace::string::operator[](size_t index)
{
	assert(index < _size);
	return _str[index];
}
// 4.2const对象的元素访问
const char& mySpace::string::operator[](size_t index)const
{
	assert(index < _size);
	return _str[index];
}

// 5.修改
// 5.1对象尾插字符
void mySpace::string::push_back(char c)
{
	if (_size == _capacity)
	{
		// 扩容
		reserve(_capacity * 2);
	}
	_str[_size] = c;
	_str[++_size] = '\0';
}
// 5.2对象拼接字符
mySpace::string& mySpace::string::operator+=(char c)
{
	push_back(c);
	return *this;
}
// 5.3对象拼接字符串
mySpace::string& mySpace::string::operator+=(const char* str)
{
	append(str);
	return *this;
}
// 5.4对象拼接字符串
void mySpace::string::append(const char* str)
{
	size_t len = strlen(str);
	// 判断是否需要扩容
	if (_size + len > _capacity)
	{
		size_t newcapacity = _capacity * 2 > (_size + len) ? _capacity * 2 : (_size + len + 1);
		reserve(newcapacity);
	}
	// 拼接字符串
	strcat(_str, str);
	_size += strlen(str);
}
// 5.5清理对象
void mySpace::string::clear()
{
	_str[0] = '\0';
	_size = 0;
}
// 5.6交换两对象
void mySpace::string::swap(mySpace::string& s)
{
	// 借助std的swap函数实现
	std::swap(_str, s._str);
	std::swap(_size, s._size);
	std::swap(_capacity, s._capacity);
}
// 5.7string对象转为char*字符串
const char* mySpace::string::c_str()const
{
	return _str;
}

// 6.其他
// 6.1返回c在string中第一次出现的位置（从pos位置开始找）
size_t mySpace::string::find(char c, size_t pos = 0) const
{
	assert(pos < _size);
	for (size_t i = pos; i < _size; i++)
	{
		if (c == _str[i])
		{
			return i;
		}
	}
	return -1;
}

// 6.2返回子串s在string中第一次出现的位置
size_t mySpace::string::find(const char* s, size_t pos) const
{
	assert(s);
	assert(pos < _size);
	const char* src = _str + pos;
	while (*src)
	{
		const char* match = s;
		const char* cur = src;
		while (*match && *match == *cur)
		{
			++match;
			++cur;
		}
		if (*match == '\0')
		{
			// 返回下标
			return src - _str;
		}
		else
		{
			// 更新条件，继续循环
			++src;
		}
	}
	return -1;
}

// 6.3在pos位置上插入字符c，并返回该字符的位置
mySpace::string& mySpace::string::insert(size_t pos, char c)
{
	assert(pos <= _size);
	// 判断是否需要扩容
	if (_size == _capacity)
	{
		reserve(_capacity * 2);
	}
	// pos后数据从后开始向后移
	for (int i = _size; i >= (int)pos; --i)
	{
		_str[i + 1] = _str[i];

	}
	_str[pos] = c;
	_size++;

	return *this;
}

// 6.4在pos位置上插入字符串str，并返回该字符的位置
mySpace::string& mySpace::string::insert(size_t pos, const char* str)
{
	assert(pos <= _size);
	size_t len = strlen(str);
	// 判断是否需要扩容
	if (_size + len > _capacity)
	{
		size_t newcapacity = _capacity * 2 > (_size + len) ? _capacity * 2 : (_size + len + 1);
		reserve(newcapacity);
	}
	// 后移数据
	for (int i = _size; i >= (int)pos; --i)
	{
		_str[len + i] = _str[i];
	}
	// 拷贝字符串
	while (*str != '\0')
	{
		_str[pos++] = *str++;
	}
	_size += len;
	return *this;
}

// 6.5删除pos位置上的元素，并返回该元素的下一个位置（从pos开始删除len个元素）
mySpace::string& mySpace::string::erase(size_t pos, size_t len)
{
	assert(pos < _size);
	// pos位置之后全为0
	if (pos + len >= _size)
	{
		_str[pos] = '\0';
		_size = pos;
	}
	else
	{
		strcpy(_str + pos, _str + pos + len);
		_size -= len;
	}
	return *this;
}

// 运算符重载
bool mySpace::string::operator<(const string& s)
{
	// 调用字符串比较接口
	int ret = strcmp(_str, s._str);
	if (ret < 0)
	{
		return true;
	}
	return false;
}
bool mySpace::string::operator<=(const string& s)
{
	return !(*this > s);
}
bool mySpace::string::operator>(const string& s)
{
	int ret = strcmp(_str, s._str);
	if (ret > 0)
	{
		return true;
	}
	return false;
}
bool mySpace::string::operator>=(const string& s)
{
	return !(*this < s);
}
bool mySpace::string::operator==(const string& s)
{
	int ret = strcmp(_str, s._str);
	if (ret == 0)
	{
		return true;
	}
	return false;
}
bool mySpace::string::operator!=(const string& s)
{
	return !(*this == s);
}

// 流插入<<、流提取>> 运算符不能重载为类成员函数
// 因此string类的流插入符号重载为友元函数
// 源文件定义友元函数，无需加friend关键字
std::ostream& mySpace::operator<<(std::ostream& _cout, const mySpace::string& s)
{
	_cout << s._str;
	return _cout;
}
std::istream& mySpace::operator>>(std::istream& _cin, mySpace::string& s)
{
	s.resize(0);
	char ch;
	while (1)
	{
		_cin.get(ch);
		if (ch == ' ' || ch == '\n')
		{
			break;
		}
		else
		{
			s += ch;
		}
	}
	return _cin;
}

void Test_string_1()
{
	// 测试构造
	mySpace::string s1("hello!");
	mySpace::string s2(s1);
	
	// 测试赋值
	mySpace::string s3;
	s3 = s1;

	// 测试容量
	s3.resize(16, 'a');
	s3.resize(5);

	// 测试访问
	char a = s3[1];
	//char b = s3[5];  //越界测试
	
	// 测试修改
	s2.push_back(' ');
	s2 += '~';
	s2 += "world";
	s2.append("!");
	s2.clear();

	// 测试其他
	int c = s1.find('l');
	s1.insert(c, 'l');
	int d = s1.find("o!");
	s1.insert(d, "000");
	s1.erase(d, 3);
	s1.erase(c);

	// 测试<<、>>
	mySpace::string s4;
	std::cin >> s4;
	std::cout << s4;

	// 测试析构
	// 自动调用
}