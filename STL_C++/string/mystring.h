#pragma once  //防止头文件重复包含

#include <iostream>
// 将模拟string类实现在自己的命名空间中
namespace mySpace
{
	class string
	{
		// 友元函数的声明：
		friend std::ostream& operator<<(std::ostream& _cout, const mySpace::string& s);
		friend std::istream& operator>>(std::istream& _cin, mySpace::string& s);

	public:
		// 指针是天然的迭代器
		typedef char* iterator;                   //将迭代器定义为char*指针

		// 所有成员函数的声明：

		// 1.构造和析构
		string(const char* s = "");               //带参构造函数
		string(const string& s);                  //拷贝构造函数
		string& operator=(const string& s);       //赋值重载函数
		~string();                                //析构函数

		// 2.迭代器——仅实现正向迭代器
		iterator begin()const;
		iterator end()const;

		// 3.容量
		size_t size()const;                       //返回对象有效元素个数
		size_t capacity()const;                   //返回对象容量大小
		bool empty()const;                        //判断对象是否为空串
		void resize(size_t n, char c = '\0');     //修改对象有效元素个数
		void reserve(size_t n);                   //修改对象容量大小

		// 4.元素访问
		char& operator[](size_t index);           //普通对象的元素访问
		const char& operator[](size_t index)const;//const对象的元素访问

		// 5.修改
		void push_back(char c);                   //对象尾插字符
		string& operator+=(char c);               //对象拼接字符
		string& operator+=(const char* str);      //对象拼接字符串
		void append(const char* str);             //对象拼接字符串
		void clear();                             //清理对象
		void swap(string& s);                     //交换两对象
		const char* c_str()const;                 //string对象转为char*字符串

		// 6.其他
		// 返回c在string中第一次出现的位置
		size_t find(char c, size_t pos) const;

		// 返回子串s在string中第一次出现的位置
		size_t find(const char* s, size_t pos = 0) const;

		// 在pos位置上插入字符c，并返回该字符的位置
		string& insert(size_t pos, char c);

		// 在pos位置上插入字符串str，并返回该字符的位置
		string& insert(size_t pos, const char* str);

		// 删除pos位置上的元素，并返回该元素的下一个位置
		string& erase(size_t pos, size_t len = 1);

		// 运算符重载
		bool operator<(const string& s);
		bool operator<=(const string& s);
		bool operator>(const string& s);
		bool operator>=(const string& s);
		bool operator==(const string& s);
		bool operator!=(const string& s);

	private:
		// 成员变量的定义：
		// string底层通过动态顺序表实现，因此其成员变量同顺序表结构一样
		char* _str;                               //指向堆的数组
		size_t _size;                             //有效元素大小
		size_t _capacity;                         //堆上数组空间大小
	};
}

// 测试函数的声明
extern void Test_string_1();